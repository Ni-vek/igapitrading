module.exports = function(grunt) {
    grunt.initConfig({
      uglify: {
        dist: {
            files: {
                'public/js/ig/ig-public-api.min.js': 'public/js/ig/ig-public-api.js',
                'public/js/functions.min.js': 'public/js/functions.js'
            }
        }
      }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify'); // load the given tasks
    grunt.registerTask('default', ['uglify']); // Default grunt tasks maps to grunt
  };