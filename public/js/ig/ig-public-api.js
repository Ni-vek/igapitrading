// Default API gateway
var urlRoot = "https://demo-api.ig.com/gateway/deal";

var url = window.location.href;

// If deployed to a labs environment, override the default demo urlRoot
var env = url.match("http:\/\/(.*)-labs.ig.com");

if (env && env.length>1) {
    var envOverride = env[1].toLowerCase();
    urlRoot = urlRoot.replace("demo", envOverride);
	//console.log("Overriding urlRoot with: " + urlRoot);
} else if (url.indexOf("localhost")>0) {
    urlRoot = "https://web-api.ig.com/gateway/deal";
    //console.log("Overriding urlRoot with: " + urlRoot);
}

// To avoid first confirm on login. Sometimes, a old cloded trade is populated on login
var avoidConfirmOnLogin = true;
// Globals variables
var isReal = false;
var accountId = null;
var account_token = null;
var apiKey = null;
var client_token = null;
var lsEndpoint = null;
var accountCurrency = null;
var accountCurrencySymbol = null;
var currentPnl = 0;
var openTrades = {};
var alarms = {};
var mySound = new buzz.sound("/sounds/thin.mp3");
var clickEventTrigger = 'mouseup';
var retrievedTransactions;
var transactionsForStatistics;
var lsClient;
var ticketSubscription;
var accountSubscription;
var accountUpdateSubscription;
var responseTimeStart = null;
var responseTimeSending = null;
var responseTimeWaiting = null;
var responseTimeStop = null;
var tradingAllowed = false;
var statsShown = false;
var timeout = null;

require(["LightstreamerClient","Subscription"],function(LightstreamerClient,Subscription) {

function getRequestParam(name){
   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
      return decodeURIComponent(name[1]);
}

/*
 * Function to connect to Lightstreamer
 */
function connectToLightstreamer() {

      // Instantiate Lightstreamer client instance
      //console.log("Connecting to Lighstreamer: " + lsEndpoint);
      lsClient = new LightstreamerClient(lsEndpoint);

      // Set up login credentials: client
      lsClient.connectionDetails.setUser(accountId);

      var password = "";
      if (client_token) {
         password = "CST-" + client_token;
      }
      if (client_token && account_token) {
         password = password + "|";
      }
      if (account_token) {
         password = password + "XST-" + account_token;
      }
      //console.log(" LSS login " + accountId + " - " + password);
      lsClient.connectionDetails.setPassword(password);

      // Add connection event listener callback functions
      lsClient.addListener({
         onListenStart: function () {
            //console.log('Lightstreamer client - start listening');
         },
         onStatusChange: function (status) {
            //console.log('Lightstreamer connection status:' + status);
             if(status == "DISCONNECTED:WILL-RETRY" || status == "STALLED"){
                 $('#connection-a').removeClass('c-lightgreen').addClass('c-orange');
                 $('#connection-icon').removeClass('zmdi-cast-connected').addClass('zmdi-cast');
                 $('#connection-li').addClass('hidden');
                 avoidConfirmOnLogin = true;
                 //Auto Close Timer
                 swal({   
                    title: "You have been disconnected!",   
                    text: "Will retry in few seconds. Please wait!",   
                    timer: 8000,   
                    showConfirmButton: false 
                });
             }
             if(status == "DISCONNECTED"){
                 $('#connection-a').removeClass('c-lightgreen').removeClass('c-orange').addClass('c-red');
                 $('#connection-icon').removeClass('zmdi-cast-connected').addClass('zmdi-cast');
                 $('#connection-li').addClass('hidden');
                 avoidConfirmOnLogin = true;
                 swal({   
                    title: "You have been disconnected!",   
                    text: "You have to log in again!",   
                    type: "warning",   
                    showCancelButton: false,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "OK, let's go!",   
                    closeOnConfirm: true 
                }, function(){   
                    window.location = "/"; 
                });
             }
             if(status == "CONNECTED:STREAM-SENSING" || status == "CONNECTED:WS-STREAMING" || status == "CONNECTED:HTTP-STREAMING" || status == "CONNECTED:WS-POLLING" || status == "CONNECTED:HTTP-STREAMING"){
                 $('#connection-a').removeClass('c-orange').addClass('c-lightgreen');
                 $('#connection-icon').removeClass('zmdi-cast').addClass('zmdi-cast-connected');
                 $('#connection-li').removeClass('hidden');
             }
         }
      });

      // Allowed bandwidth in kilobits/s
      //lsClient.connectionOptions.setMaxBandwidth();

      // Connect to Lightstreamer
      lsClient.connect();
}

function subscribeToLightstreamerTradeUpdates() {
      // Set up the Lightstreamer FIDs
      accountSubscription = new Subscription(
         "DISTINCT",
         "TRADE:" + accountId,
         [
            "CONFIRMS",
            "OPU",
            "WOU"
         ]
      );

      accountSubscription.setRequestedMaxFrequency("unfiltered");

      // Set up the Lightstreamer event listeners
      accountSubscription.addListener({
         onSubscription: function () {
            //console.log('trade updates subscription succeeded');
         },
         onSubscriptionError: function (code, message) {
            //console.log('trade updates subscription failure: ' + code + " message: " + message);
         },
         onItemUpdate: function (updateInfo) {

            //console.log("received trade update message: " + updateInfo.getItemName());
             if(!avoidConfirmOnLogin){
                 updateInfo.forEachField(function (fieldName, fieldPos, value) {
                    //console.log("fieldName: " +fieldName);
                    //console.log("value: " +value);
                   if (value != 'INV') {
                      if (fieldName == "CONFIRMS" || (fieldName == "OPU")) {
                         showDealConfirmDialog(value);
                      }
                       else if (fieldName == "CONFIRMS") {

                       }
                       else {
                         showAccountStatusUpdate(value);
                      }
                   }
                });
             }
             else{
                 avoidConfirmOnLogin = false;
             }
         },
         onItemLostUpdates: function () {
            //console.log("trade updates subscription - item lost");
         }

      });

      // Subscribe to Lightstreamer
      lsClient.subscribe(accountSubscription);
}
    
/*
 * Account updates subscription
 */
function subscribeToAccountUpdates(){
    // Set up the Lightstreamer FIDs
      accountUpdateSubscription = new Subscription(
         "MERGE",
         "ACCOUNT:" + accountId,
         [
            "PNL",
            "DEPOSIT",
            "AVAILABLE_CASH",
            "FUNDS",
            "MARGIN",
            "AVAILABLE_TO_DEAL",
            "EQUITY",
            "EQUITY_USED",
         ]
      );

      accountUpdateSubscription.setRequestedSnapshot("yes");

      // Set up the Lightstreamer event listeners
      accountUpdateSubscription.addListener({
         onSubscription: function () {
            //console.log('account updates subscription succeeded');
         },
         onSubscriptionError: function (code, message) {
            //console.log('account updates subscription failure: ' + code + " message: " + message);
         },
         onItemUpdate: function (updateInfo) {
             updateInfo.forEachField(function (fieldName, fieldPos, value) {
                //console.log("fieldName: " +fieldName);
                //console.log("value: " +value);
                 var cell = $('#' + fieldName);
                  if (cell.length) {
                     cell.html(value);
                  }
                 if(fieldName == "PNL"){
                     if(value > 0){
                         $('#PNLCColor').removeClass('c-deeporange').addClass('c-lightblue');
                     }
                     else if(value == 0){
                         $('#PNLCColor').removeClass('c-deeporange').removeClass('c-lightblue');
                     }
                     else {
                         $('#PNLCColor').addClass('c-deeporange').removeClass('c-lightblue');
                     }
                 }
            });
         },
         onItemLostUpdates: function () {
            //console.log("account updates subscription - item lost");
         }

      });

      // Subscribe to Lightstreamer
      lsClient.subscribe(accountUpdateSubscription);
}

/**
 * Encryption function
 */

function encryptedPassword(password) {
    try{
        var key = encryptionKey();

        var asn, tree,
        rsa = new pidCrypt.RSA(),
        decodedKey = pidCryptUtil.decodeBase64(key.encryptionKey);

        asn = pidCrypt.ASN1.decode(pidCryptUtil.toByteArray(decodedKey));
        tree = asn.toHexTree();

        rsa.setPublicKeyFromASN(tree);

        return pidCryptUtil.encodeBase64(pidCryptUtil.convertFromHex(rsa.encrypt(password += '|' + key.timeStamp)))
    } catch(e){
        handleException(e);
        return false;
    }
}

/**
 * Encryption key getter function
 */
function encryptionKey() {


        // Set up the request as a GET request to the address /session/encryptionkey
        var req = new Request();
        req.method = "GET";
        req.url = urlRoot + "/session/encryptionKey";

        // Set up the request headers, i.e. the api key, the account security session token, the client security token,
        // the request content type (JSON), and the expected response content type (JSON)
        req.headers = {
            "X-IG-API-KEY": apiKey,
            "Content-Type": "application/json; charset=UTF-8",
            "Accept": "application/json; charset=UTF-8"
        };

        // No body is required, as this is a GET request
        req.body = "";
        //$("#request_data").text(js_beautify(req.body) || "");

        // Send the request via a Javascript AJAX call
        var key;
        try {
            $.ajax({
                type: req.method,
                url: req.url,
                data: req.body,
                headers: req.headers,
                async: false,
                mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                error: function (response, status, error) {
                    handleHTTPError(response);
                },
                success: function (response, status, data) {

                    //console.log("Encryption key retrieved ");
                    key = response;
                }
            });
        } catch (e) {

            // Failed to get the encryption key
            handleException(e);
        }

        return key;

}

/*
 * Function to retrieve the deal confirmation for the given deal reference
 */
function retrieveConfirm(dealReference) {

   // Set up the request as a GET request to the address /confirms
   var req = new Request();
   req.method = "GET";
   req.url = urlRoot + "/confirms/" + dealReference;

   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8"
   };

   // No body is required, as this is a GET request
   req.body = "";
   //$("#request_data").text(js_beautify(req.body) || "");

   // Send the request via a Javascript AJAX call
   var message;
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         data: req.body,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
            handleHTTPError(response);
         },
         success: function (response, status, data) {

            // Got confirm data back
            // Prettify response for display purposes only
            //$("#response_data").text(js_beautify(data.responseText) || "");

            // Log and set the confirm for later display
            //console.log("confirm retrieved");
            message = response;
         }
      });
   } catch (e) {

      // Failed to get the confirmation, possibly because the deal reference was not matched to any deal
      handleException(e);
   }

   return message;
}

/*
 * Function to retrieve the positions for the active account
 */
function retrievePositions(onSubscription) {

   // Set up the request as a GET request to the address /positions
   var req = new Request();
   req.method = "GET";
   req.url = urlRoot + "/positions";

   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)   
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8"
   };

   // No body is required, as this is a GET request
   req.body = "";
   //$("#request_data").text(js_beautify(req.body) || "");

   // Send the request via a Javascript AJAX call
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         data: req.body,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
            // An unexpected error occurred
            handleHTTPError(response);
         },
         success: function (response, status, data) {

             if(!onSubscription){
                 populatePositionsOnLogin(response);
             }
             else{
                 populatePositionsOnSubscription(response);
             }
         }
      });
   } catch (e) {
      handleException(e);
   }
}

/*
 * Function to search for markets against which to trade
 */
function search() {

   // Set up the request as a GET request to the address /markets with a search query parameter of ?searchterm={searchterm}
   var searchTerm = $("#searchEpic").val();
   var req = new Request();
   req.method = "GET";
   req.url = urlRoot + "/markets?searchTerm=" + searchTerm;

   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8"
   };

   // No body is required, as this is a GET request
   req.body = "";
   //$("#request_data").text(js_beautify(req.body) || "");

   // Send the request via a Javascript AJAX call
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         data: req.body,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
            // Something went wrong
            handleHTTPError(response);
         },
         success: function (response, status, data) {

            // A search result was returned
            // Prettify the response for display purposes only
            //$("#response_data").text(js_beautify(data.responseText) || "");

            // Log and display the search results, along with the Lightstreamer subscription FIDs for the BID and OFFER
            // price of each market returned
            $('#search_results_list tbody').empty();
              var epicsItems = [];
              $('#tbodyepicslist').empty();

            $(response.markets).each(function (index) {
                
               var marketsData = response.markets[index];
               var epic = marketsData.epic;
               var canSubscribe = marketsData.streamingPricesAvailable;
               var tidyEpic = epic.replace(/\./g, "_");
               var expiry = marketsData.expiry.replace(/ /g, "");
               var linkId = "searchResult_" + tidyEpic;
               $('#tbodyepicslist').append('<tr><td>' + marketsData.instrumentName + '</td><td>' + expiry + '</td><td><button id="' + marketsData.epic + '" class="btn btn-primary waves-effect newepic" data-epic="L1:' + marketsData.epic + '">+</button></td></tr>');

               $('#' + linkId).on(clickEventTrigger, function () {
                  dealTicket(epic, expiry);
               });

               if (canSubscribe) {
                  var epicsItem = "L1:" + marketsData.epic;
                  epicsItems.push(epicsItem);
               }

               return index < 39;
            });
            $('#modalEpic').modal();
            $('.newepic').on('click', function(){
                if(!$('#HEADER_' + $(this).attr('data-epic').split(":")[1].replace(/\./g, "_")).length){
                    subscription($(this).attr('data-epic'));
                }
            });
            //console.log(epicsItems);
         }
      });
   } catch (e) {
      handleException(e);
   }
}
    
/*
 * Remove subscription
 */
function unsubscribe(subscription){
    if($('div[data-subscription]').length == 0){
        $('#nocardjumbotron').removeClass('hidden');
    }
    lsClient.unsubscribe(subscription);
}
    
/*
 * Subscription
 */
function subscription(epicItem){
    // Now subscribe to the BID and OFFER prices for each market found
    if (epicItem.length > 0) {

          // Set up Lightstreamer FIDs
          var subscription = new Subscription(
             "MERGE",
             epicItem,
             [
                "BID",
                "OFFER",
                "MARKET_STATE"
             ]
          );

           subscription.setRequestedSnapshot("yes");

          // Set up Lightstreamer event listener
          subscription.addListener({
             onSubscription: function () {
                 if (!$('#nocardjumbotron').hasClass('hidden')){
                     $('#nocardjumbotron').addClass('hidden');
                 }
                 var newepicinfo = marketDetails(epicItem.split(":")[1]);
                 //console.log(newepicinfo);
                 var sentiment = marketSentiment(newepicinfo.instrument.marketId);
                 //console.log(sentiment);
                 var newepic = '<div class="col-sm-6" id="' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-subscription>'
                                    + '<div class="card">'
                                        + '<div id="HEADER_' + epicItem.split(":")[1].replace(/\./g, "_") + '" class="card-header bgm-bluegray">'
                                            + '<h2><a href="#" id="FAVORITE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem + '" data-name="' + newepicinfo.instrument.name + '" class="' + (epicItem in settings.favorite_epics ? 'favoriteStarTrue' : 'favoriteStar') + '"><i id="IS_FAVORITE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" class="zmdi ' + (epicItem in settings.favorite_epics ? 'zmdi-star' : 'zmdi-star-outline') + '"></i> </a>' + newepicinfo.instrument.name + ' <span id="LOTS_' + epicItem.split(":")[1].replace(/\./g, "_") + '" class="lots-size"></span></h2>'
                                            + '<ul class="actions">'
                                                + '<li>'
                                                    + '<a href="#" id="REMOVE_SUBSCRIPTION_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1].replace(/\./g, "_") + '"><i class="zmdi zmdi-close"></i></a>'
                                                + '</li>'
                                                + '<li class="dropdown">'
                                                    + '<a href="#" data-toggle="dropdown">'
                                                        + '<i class="zmdi zmdi-more-vert"></i>'
                                                    + '</a>'
                                                    + '<ul class="dropdown-menu dropdown-menu-right">'
                                                        + '<li>'
                                                            + '<a href="#" id="ADD_ALARM_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic-name="' + newepicinfo.instrument.name + '">Add / update alarm</a>'
                                                            + '<a href="#" id="UPDATE_SENTIMENT_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-market="' + newepicinfo.instrument.marketId + '">Update sentiment</a>'
                                                            + '<a href="#" id="MINI_MODE_TOGGLE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic-name="' + newepicinfo.instrument.name + '">Toggle Mini-mode</a>'
                                                        + '</li>'
                                                    + '</ul>'
                                                + '</li>'
                                            + '</ul>'
                                        + '</div>'
                                        + (newepicinfo.snapshot.marketStatus != 'TRADEABLE' ? '<div class="bgm-indigo text-center f-27 p-10 c-white f-700">MARKET CLOSED</div>' : '')
                                        + '<div id="MARKET_SENTIMENT_' + epicItem.split(":")[1].replace(/\./g, "_") + '">'
                                            + '<div class="progress" style="height: 18px">'
                                                + '<div id="MARKET_SENTIMENT_SELL_' + epicItem.split(":")[1].replace(/\./g, "_") + '" class="progress-bar progress-bar-danger" style="width: ' + sentiment.shortPositionPercentage + '%">'
                                                    + '<span id="MARKET_SENTIMENT_SELL_VALUE_' + epicItem.split(":")[1].replace(/\./g, "_") + '">' + sentiment.shortPositionPercentage + '%</span>'
                                                + '</div>'
                                                + '<div id="MARKET_SENTIMENT_BUY_' + epicItem.split(":")[1].replace(/\./g, "_") + '" class="progress-bar progress-bar-info" style="width: ' + sentiment.longPositionPercentage + '%">'
                                                    + '<span id="MARKET_SENTIMENT_BUY_VALUE_' + epicItem.split(":")[1].replace(/\./g, "_") + '">' + sentiment.longPositionPercentage + '%</span>'
                                                + '</div>'
                                            + '</div>'
                                        + '</div>'
                                        + '<div id="ALARM_ICONS_' + epicItem.split(":")[1].replace(/\./g, "_") + '" class="card-header">'
                                        + '</div>'
                                        + '<div class="card-body m-t-0">'
                                            + '<div id="MINI_MODE_' + epicItem.split(":")[1].replace(/\./g, "_") + '">'
                                                + '<div class="row m-t-5 text-center">'
                                                    + '<div class="col-xs-12">'
                                                        + '<div class="col-xs-4">'
                                                            + 'Spread: <span id="SPREAD_' + epicItem.split(":")[1].replace(/\./g, "_") + '"></span>'
                                                        + '</div>'
                                                        + '<div class="col-xs-4">'
                                                            + 'Stop min: <span id="STOP_MIN_' + epicItem.split(":")[1].replace(/\./g, "_") + '">' 
                                                            + newepicinfo.dealingRules.minNormalStopOrLimitDistance.value + '</span>' 
                                                        + '</div>'
                                                        + '<div class="col-xs-4">'
                                                            + 'Stop guaranted: <span id="STOP_GUARANTED_' + epicItem.split(":")[1].replace(/\./g, "_") + '">' 
                                                            + newepicinfo.dealingRules.minControlledRiskStopDistance.value + '</span>'
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="row m-t-5 text-center">'
                                                    + '<div class="col-xs-12">'
                                                        + '<div class="col-xs-6">'
                                                            + '<div class="checkbox">'
                                                                + '<label>'
                                                                    + '<input type="checkbox" value="" id="FORCE_OPEN_' + epicItem.split(":")[1].replace(/\./g, "_") + '" ' + (!newepicinfo.instrument.forceOpenAllowed ? 'disabled' : 'checked') + '>'
                                                                    + '<i class="input-helper"></i>'
                                                                    + 'Force Open'
                                                                + '</label>'
                                                            + '</div> '
                                                        + '</div>'
                                                        + '<div class="col-xs-6">'
                                                            + '<div class="checkbox">'
                                                                + '<label>'
                                                                    + '<input type="checkbox" value="" id="GUARANTED_STOP_' + epicItem.split(":")[1].replace(/\./g, "_") + '"' + (!newepicinfo.instrument.controlledRiskAllowed ? ' disabled' : '') + '>'
                                                                    + '<i class="input-helper"></i>'
                                                                    + 'Guaranted Stop'
                                                                + '</label>'
                                                            + '</div> '
                                                        + '</div>'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="row m-t-5 text-center">'
                                                    + '<div class="col-xs-12">'
                                                        + '<div class="col-xs-4">'
                                                            + '<div class="form-group fg-line">'
                                                                + '<label for="SIZE_' + epicItem.split(":")[1].replace(/\./g, "_") + '">Size (min: <span id="MIN_LOTS_' + epicItem.split(":")[1].replace(/\./g, "_") + '">' + newepicinfo.dealingRules.minDealSize.value + '</span>)</label>'
                                                                + '<input type="text" class="form-control input-sm" id="SIZE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" placeholder="Size" value="' + newepicinfo.dealingRules.minDealSize.value + '">'
                                                            + '</div>'
                                                        + '</div>'
                                                        + '<div class="col-xs-4">'
                                                            + '<div class="form-group fg-line">'
                                                                + '<label for="SL_' + epicItem.split(":")[1].replace(/\./g, "_") + '">SL (points)</label>'
                                                                + '<input type="text" class="form-control input-sm" id="SL_' + epicItem.split(":")[1].replace(/\./g, "_") + '" placeholder="SL (points)" value="" ' + (!newepicinfo.instrument.stopsLimitsAllowed ? ' disabled' : '') + '>'
                                                            + '</div> '
                                                        + '</div>'
                                                        + '<div class="col-xs-4">'
                                                            + '<div class="form-group fg-line">'
                                                                + '<label for="LL_' + epicItem.split(":")[1].replace(/\./g, "_") + '">LL (points)</label>'
                                                                + '<input type="text" class="form-control input-sm" id="LL_' + epicItem.split(":")[1].replace(/\./g, "_") + '" placeholder="LL (points)" value="" ' + (!newepicinfo.instrument.stopsLimitsAllowed ? ' disabled' : '') + '>'
                                                            + '</div>  '
                                                        + '</div>'
                                                        + '<input type="hidden" id="EXPIRY_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="' 
                                                            + newepicinfo.instrument.expiry + '">'
                                                        + '<input type="hidden" id="EPIC_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="' 
                                                            + epicItem.split(":")[1] + '">'
                                                        + '<input type="hidden" id="EPIC_CURRENCY_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="' 
                                                            + newepicinfo.instrument.currencies[0].code + '">'
                                                        + '<input type="hidden" id="EPIC_POINT_VAL_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="' 
                                                            + newepicinfo.instrument.contractSize + '">'
                                                        + '<input type="hidden" id="EPIC_BASE_EXCHANGE_RATE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="' 
                                                            + newepicinfo.instrument.currencies[0].baseExchangeRate + '">'
                                                    + '</div>'
                                                + '</div>'
                                            + '</div>'
                                            + '<div class="row m-t-5">'
                                                + '<div class="col-xs-12">'
                                                    + '<button class="col-xs-12 p-t-15 p-b-15 btn btn-warning waves-effect f-17" id="CLOSE_ALL_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1] + '">Close all "' + newepicinfo.instrument.name + '"</button>'
                                                + '</div>'
                                            + '</div>'
                                            + '<div class="row m-t-5">'
                                                + '<div class="col-xs-12">'
                                                    + '<button class="col-xs-4 btn bgm-gray waves-effect" id="SL_TO_0_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1] + '">SL to 0</button>'
                                                    + '<button class="col-xs-4 btn bgm-gray waves-effect" id="SL_TO_PRU_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1] + '">SL to PRU (<span id="PRU_' + epicItem.split(":")[1].replace(/\./g, "_") + '"></span>)</button>'
                                                    + '<button class="col-xs-4 btn bgm-gray waves-effect" id="LL_TO_0_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1] + '">LL to 0</button>'
                                                + '</div>'
                                            + '</div>'
                                            + '<div class="row m-t-5">'
                                                + '<div class="col-xs-12">'
                                                    + '<input type="hidden" id="BID_VALUE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="">'
                                                    + '<input type="hidden" id="OFFER_VALUE_' + epicItem.split(":")[1].replace(/\./g, "_") + '" value="">'
                                                    + '<button class="col-xs-6 p-t-15 p-b-15 btn btn-danger waves-effect f-17" id="BID_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1].replace(/\./g, "_") + '"' + (newepicinfo.snapshot.marketStatus != 'TRADEABLE' ? ' disabled' : '') + '>' + newepicinfo.snapshot.bid + '</button>'
                                                    + '<button class="col-xs-6 p-t-15 p-b-15 btn btn-primary waves-effect f-17" id="OFFER_' + epicItem.split(":")[1].replace(/\./g, "_") + '" data-epic="' + epicItem.split(":")[1].replace(/\./g, "_") + '"' + (newepicinfo.snapshot.marketStatus != 'TRADEABLE' ? ' disabled' : '') + '>' + newepicinfo.snapshot.offer + '</button>'
                                                + '</div>'
                                            + '</div>'
                                            + '<div class="m-t-5">'
                                                + '<table class="table table-hover table-inner table-vmiddle table-striped">'
                                                    + '<thead>'
                                                        + '<tr>'
                                                            + '<th>Dir</th>'
                                                            + '<th>Size</th>'
                                                            + '<th>Prices</th>'
                                                            + '<th>Points</th>'
                                                        + '</tr>'
                                                    + '</thead>'
                                                    + '<tbody id="OPEN_TRADES_' + epicItem.split(":")[1].replace(/\./g, "_") + '">'
                                                    + '</tbody>'
                                                + '</table>'
                                            + '</div>'
                                        + '</div>'
                                    + '</div>'
                                + '</div>';
                 if(!$('#HEADER_' + epicItem.split(":")[1].replace(/\./g, "_")).length){
                     $('#epicTradingCard').append(newepic);
                     $('#REMOVE_SUBSCRIPTION_' + epicItem.split(":")[1].replace(/\./g, "_")).on('click', function(e){
                         e.preventDefault();
                         $('#' + $(this).attr('data-epic')).remove();
                     });
                     $('#ADD_ALARM_' + epicItem.split(":")[1].replace(/\./g, "_")).on('click', function(e){
                         e.preventDefault();
                         addAlarm($(this).attr('data-epic').replace(/\_/g, "."), $(this).attr('data-epic-name'));
                     });
                     $('#UPDATE_SENTIMENT_' + epicItem.split(":")[1].replace(/\./g, "_")).on('click', function(e){
                         e.preventDefault();
                         updateSentiment($(this).attr('data-market'), epicItem.split(":")[1].replace(/\./g, "_"), true);
                     });
                     $('#BID_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(){
                         responseTimeStart = new Date();
                         var epicBase = $(this).attr('data-epic');
                         placeTrade($('#EPIC_' + epicBase).val(), $('#EXPIRY_' + epicBase).val(), $('#SIZE_' + epicBase).val(), $('#BID_VALUE_' + epicBase).val(), $('#OFFER_VALUE_' + epicBase).val(), 'SELL', $('#FORCE_OPEN_' + epicBase).prop('checked'), $('#GUARANTED_STOP_' + epicBase).prop('checked'), $('#SL_' + epicBase).val(), $('#LL_' + epicBase).val(), $('#EPIC_CURRENCY_' + epicBase).val());
                     });
                     $('#OFFER_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(){
                         responseTimeStart = new Date();
                         var epicBase = $(this).attr('data-epic');
                         placeTrade($('#EPIC_' + epicBase).val(), $('#EXPIRY_' + epicBase).val(), $('#SIZE_' + epicBase).val(), $('#BID_VALUE_' + epicBase).val(), $('#OFFER_VALUE_' + epicBase).val(), 'BUY', $('#FORCE_OPEN_' + epicBase).prop('checked'), $('#GUARANTED_STOP_' + epicBase).prop('checked'), $('#SL_' + epicBase).val(), $('#LL_' + epicBase).val(), $('#EPIC_CURRENCY_' + epicBase).val());
                     });
                     $('#CLOSE_ALL_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(){
                         responseTimeStart = new Date();
                         closeAllEpicTrades($(this).attr('data-epic'));
                     });
                     $('#SL_TO_0_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(){
                         responseTimeStart = new Date();
                         updateTrades($(this).attr('data-epic'), "SL_TO_0");
                     });
                     $('#SL_TO_PRU_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(){
                         responseTimeStart = new Date();
                         updateTrades($(this).attr('data-epic'), "SL_TO_PRU");
                     });
                     $('#LL_TO_0_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(){
                         responseTimeStart = new Date();
                         updateTrades($(this).attr('data-epic'), "LL_TO_0");
                     });
                     $('#MINI_MODE_TOGGLE_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(e){
                         e.preventDefault();
                         $('#MINI_MODE_' + epicItem.split(":")[1].replace(/\./g, "_")).toggle();
                     });
                     $('#FAVORITE_' + epicItem.split(":")[1].replace(/\./g, "_")).on(clickEventTrigger, function(e){
                         e.preventDefault();
                         toggleFavoriteEpic($(this).attr('data-epic'), $(this).attr('data-name'));
                     });
                     setInterval(function(){
                         updateSentiment(newepicinfo.instrument.marketId, epicItem.split(":")[1].replace(/\./g, "_"), true);
                     }, 3e5)
                 }
                 //console.log('subscribed!');
             },
             onSubscriptionError: function (code, message) {
                //console.log('subscription failure: ' + code + " message: " + message);
             },
             onItemUpdate: function (updateInfo) {
                 //console.log(updateInfo);
                // Lightstreamer published some data
                // The item name in this case will be the market EPIC for which prices were subscribed to
                var epic = updateInfo.getItemName().split(":")[1];
                var tidyEpic = epic.replace(/\./g, "_");
                 var cell = $('#BID_' + tidyEpic);
                 if(cell.length){  
                     $('#BID_' + tidyEpic).html(updateInfo.Dbi[2]);
                     $('#OFFER_' + tidyEpic).html(updateInfo.Dbi[3]);
                     $('#BID_VALUE_' + tidyEpic).val(updateInfo.Dbi[2]);
                     $('#OFFER_VALUE_' + tidyEpic).val(updateInfo.Dbi[3]);
                     var lots_size = (Math.round(+$('#FUNDS').html() * settings.lots_size.percent / 100 / settings.lots_size.stop / ($('#EPIC_POINT_VAL_' + tidyEpic).val() / $('#EPIC_BASE_EXCHANGE_RATE_' + tidyEpic).val()) * 100) / 100);
                     if(settings.lots_size.autocalculate){
                         $('#LOTS_' + tidyEpic).html(' [' + lots_size + ']');
                     }
                     else{
                         $('#LOTS_' + tidyEpic).empty();
                     }
                     if(settings.lots_size.autocalculate && settings.lots_size.autofill){
                         if(lots_size > +$('#MIN_LOTS_' + tidyEpic).html()){
                             $('#SIZE_' + tidyEpic).val(lots_size.toFixed(1));
                         }
                         else{
                             $('#SIZE_' + tidyEpic).val(+$('#MIN_LOTS_' + tidyEpic).html());
                         }
                     }
                     else{
                         +$('#MIN_LOTS_' + tidyEpic).html();
                     }
                     $('#SPREAD_' + tidyEpic).html(Math.round((updateInfo.Dbi[3] - updateInfo.Dbi[2])*100) / 100);
                     if($('#OPEN_TRADES_' + epicItem.split(":")[1].replace(/\./g, "_") + ' > tr').length){
                         $('#OPEN_TRADES_' + epicItem.split(":")[1].replace(/\./g, "_") + ' > tr').each(function(){
                             var dealId = $(this).attr('id');
                             //console.log(dealId);
                             var points = 0;
                             
                             // Calculate points
                             if($('#SIZE_'+ dealId).html() >= 0){
                                 points = (Math.round((updateInfo.Dbi[2] - $('#OPEN_PRICE_' + dealId).html()) * 100) / 100);
                             }
                             else{
                                 points = (Math.round(($('#OPEN_PRICE_' + dealId).html() - updateInfo.Dbi[3]) * 100) / 100);
                             }
                             
                             // Update trade points value
                             $('#POINTS_' + dealId).html(points);
                             openTrades[dealId] = points;
                             
                             // Update total current pnl
                             currentPnl = 0;
                             for(var dealIdOpen in openTrades){
                                 currentPnl += openTrades[dealIdOpen];
                             }
                             $('#PNLCP').html(Math.round(currentPnl * 100) / 100);
                             if(points > 0){
                                 $('#' + dealId).removeClass('c-red').removeClass('c-green').addClass('c-lightblue');
                             }
                             else if(points === 0){
                                 $('#' + dealId).removeClass('c-red').addClass('c-green').removeClass('c-lightblue');
                             }
                             else {
                                 $('#' + dealId).addClass('c-red').removeClass('c-green').removeClass('c-lightblue');
                             }
                             if(currentPnl > 0){
                                 $('#PNLCPColor').removeClass('c-deeporange').addClass('c-lightblue');
                             }
                             else if(currentPnl == 0){
                                 $('#PNLCPColor').removeClass('c-deeporange').removeClass('c-lightblue');
                             }
                             else {
                                 $('#PNLCPColor').addClass('c-deeporange').removeClass('c-lightblue');
                             }
                         });
                     }
                     runAlarms(epic, updateInfo.Dbi[2], updateInfo.Dbi[3]);
                 }
                 else{
                     unsubscribe(subscription);
                 }
                 
                /*updateInfo.forEachField(function (fieldName, fieldPos, value) {
                   var fieldId = fieldName + '.' + epic;
                   var cell = $("#" + fieldId.replace(/\./g, "_"));
                   if (fieldName === "MARKET_STATE") {
                      //update status image
                      if (value == "TRADEABLE") {
                         cell.attr("src", "/img/open.png")
                      } else if (value == "EDIT") {
                         cell.attr("src", "/img/edit.png")
                      } else {
                         cell.attr("src", "/img/close.png")
                      }
                   } else {
                       //console.log(cell);
                       if(cell.length){
                            cell.html(value);
                            //console.log('#' + cell);
                       }
                       else{
                           unsubscribe(subscription);
                       }
                   }
                    
                });*/
             }
          });
        
          //Retrieve open postions
          retrievePositions(true);
          // Subscribe to Lightstreamer
          lsClient.subscribe(subscription);
    }
}
    
/*
 * Toggle epic in favorite epics
 */
function toggleFavoriteEpic(epic, name){
    if(epic in settings.favorite_epics){
        delete settings.favorite_epics[epic];
        $('#IS_FAVORITE_' + epic.split(":")[1].replace(/\./g, "_")).removeClass('zmdi-star').addClass('zmdi-star-outline');
        $('#FAVORITE_' + epic.split(":")[1].replace(/\./g, "_")).removeClass('favoriteStarTrue').addClass('favoriteStar');
        saveSettings(true);
    }
    else{
        settings.favorite_epics[epic] = name;
        $('#IS_FAVORITE_' + epic.split(":")[1].replace(/\./g, "_")).addClass('zmdi-star').removeClass('zmdi-star-outline');
        $('#FAVORITE_' + epic.split(":")[1].replace(/\./g, "_")).addClass('favoriteStarTrue').removeClass('favoriteStar');
        saveSettings(true);
    }
}
    
/*
 * Add alarm
 */
function addAlarm(epic, epicName){
    $('#alarmEpicName').val(epicName);
    $('#alarmEpic').val(epic);
    $('#modalAlarm').modal('show');
}
    
function saveAlarm(){
    var newAlarm = {
        'epic': $('#alarmEpic').val(),
        'epicName': $('#alarmEpicName').val(),
        'alarmType': ($('input[name=alarmType]:checked', '#alarmForm').val() == 'LEVEL' ? 
                            $('input[name=alarmType]:checked', '#alarmForm').val() + '-' + $('#LEVEL').val() + '-' + $('#LEVEL_MARGIN').val() : 
                            $('input[name=alarmType]:checked', '#alarmForm').val() == 'PP_LEVEL' ?
                                    $('input[name=alarmType]:checked', '#alarmForm').val() + '-' + $('#PP_LEVEL_METHOD').val() + '-' + $('#PP_LEVEL_PERIOD').val() + '-' + $('#PP_LEVEL_MARGIN').val() : 
                                    $('input[name=alarmType]:checked', '#alarmForm').val()),
        'active': $('#alarmActive').prop('checked'),
        'playSound': $('#alarmPlaySound').prop('checked'),
        'soundPlayed': false
    }
    //console.log(newAlarm.alarmType);
    if(newAlarm.alarmType == '0_50_LEVEL'){
        if($('#0_50_LEVEL_MARGIN').val()){
            newAlarm['0_50_LEVEL_MARGIN'] = +$('#0_50_LEVEL_MARGIN').val();
            processAlarmUi(newAlarm);
        }
        else{
            notify('Margin is required!', 'danger');
            return;
        }
    }
    if(newAlarm.alarmType == 'LEVEL' + '-' + $('#LEVEL').val() + '-' + $('#LEVEL_MARGIN').val()){
        if($('#LEVEL').val() && $('#LEVEL_MARGIN').val()){
            newAlarm['LEVEL'] = +$('#LEVEL').val();
            newAlarm['LEVEL_MARGIN'] = +$('#LEVEL_MARGIN').val();
            processAlarmUi(newAlarm);
        }
        else{
            notify('Fileds Level and Margin are required!', 'danger');
            return;
        }
    }
    if(newAlarm.alarmType == 'PP_LEVEL' + '-' + $('#PP_LEVEL_METHOD').val() + '-' + $('#PP_LEVEL_PERIOD').val() + '-' + $('#PP_LEVEL_MARGIN').val()){
        if($('#PP_LEVEL_MARGIN').val()){
            newAlarm['PP_LEVEL_METHOD'] = $('#PP_LEVEL_METHOD').val();
            newAlarm['PP_LEVEL_PERIOD'] = $('#PP_LEVEL_PERIOD').val();
            newAlarm['PP_LEVEL_MARGIN'] = +$('#PP_LEVEL_MARGIN').val();
        }
        else{
            notify('Margin is required!', 'danger');
            return;
        }
        
       // Create a GET request to /prices/{epic}/{resolution}/{numPoints}
       var req = new Request();
        
        var today = new Date();
        var yesterday = new Date();
        if(today.getDate() == 1){
            yesterday.setDate(today.getDate() - 4);
        }else{
            yesterday.setDate(today.getDate() - 2);
        }
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        var ddy = yesterday.getDate();
        var mmy = yesterday.getMonth()+1; //January is 0!
        var yyyyy = yesterday.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 
        if(mm<10) {
            mm='0'+mm
        } 
        if(ddy<10) {
            ddy='0'+ddy
        } 
        if(mmy<10) {
            mmy='0'+mmy
        } 

        today = yyyy+'-'+mm+'-'+dd;
        yesterday = yyyyy+'-'+mmy+'-'+ddy;
        //console.log(today);
        //console.log(yesterday);
       req.method = 'GET';
       req.url = urlRoot + '/prices/' + newAlarm.epic + '/' + (newAlarm.PP_LEVEL_PERIOD == 'DAY' ?
                                                                    'DAY/' + yesterday + ' 23:00:00/' + today + ' 00:00:00' : 
                                                                    (newAlarm.PP_LEVEL_PERIOD == 'WEEK') ?
                                                                            'WEEK/2' : 
                                                                            'MONTH/2');

       // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
       // the request content type (JSON), and the expected response content type (JSON)      
       req.headers = {
          "X-IG-API-KEY": apiKey,
          "X-SECURITY-TOKEN": account_token,
          "CST": client_token,
          "Content-Type": "application/json; charset=UTF-8",
          "Accept": "application/json; charset=UTF-8",
          "Version": "2"
       };


           // Send the request via a Javascript AJAX call
           var resultData;
           try {
              $.ajax({
                 type: req.method,
                 url: req.url,
                 headers: req.headers,
                 async: true,
                 mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                 error: function (response, status, error) {
                    // Something went wrong
                    handleHTTPError(response);
                 },
                 success: function (response, status, data) {
                    
                     var O = +((response.prices[0].openPrice.ask + response.prices[0].openPrice.bid) / 2);
                     var H = +((response.prices[0].highPrice.ask + response.prices[0].highPrice.bid) / 2);
                     var L = +((response.prices[0].lowPrice.ask + response.prices[0].lowPrice.bid) / 2);
                     var C = +((response.prices[0].closePrice.ask + response.prices[0].closePrice.bid) / 2);
                     var PP = (L + H + C) / 3;
                     var S1 = (PP * 2) - H;
                     var S2 = PP - (H - L);
                     var S3 = L - 2 * (H - PP);
                     var R1 = (PP * 2) - L;
                     var R2 = PP + (H - L);
                     var R3 = H + 2 * (PP - L);
                     var mS1 = (PP + S1) / 2;
                     var mS2 = (S1 + S2) / 2;
                     var mS3 = (S2 + S3) / 2;
                     var mR1 = (PP + R1) / 2;
                     var mR2 = (R1 + R2) / 2;
                     var mR3 = (R2 + R3) / 2;
                     
                     //console.log('open: ' + O);
                     //console.log('high: ' + H);
                     //console.log('low: ' + L);
                     //console.log('close: ' + C);
                     //console.log('pivot: ' + PP.toFixed(1));
                     //console.log('s1: ' + S1.toFixed(1));
                     //console.log('s2: ' + S2.toFixed(1));
                     //console.log('s3: ' + S3.toFixed(1));
                     //console.log('r1: ' + R1.toFixed(1));
                     //console.log('r2: ' + R2.toFixed(1));
                     //console.log('r3: ' + R3.toFixed(1));
                     //console.log('ms1: ' + mS1.toFixed(1));
                     //console.log('ms2: ' + mS2.toFixed(1));
                     //console.log('ms3: ' + mS3.toFixed(1));
                     //console.log('mr1: ' + mR1.toFixed(1));
                     //console.log('mr2: ' + mR2.toFixed(1));
                     //console.log('mr3: ' + mR3.toFixed(1));
                     //console.log(response);
                     resultData = response;
                 }
              });
           } catch (e) {
              handleException(e);
           }
        
    }
}
    
/*
 * Process alarm UI
 */
function processAlarmUi(newAlarm){
        
    newAlarm['alarmTypeName'] = ($('input[name=alarmType]:checked', '#alarmForm').val() == '0_50_LEVEL' ? 
                            '00 - 50 Level (Margin: ' + newAlarm['0_50_LEVEL_MARGIN'] + ')' :
                                ($('input[name=alarmType]:checked', '#alarmForm').val() == 'PP_LEVEL' ?
                                    'Pivot Points Level' : 'Level: ' + newAlarm['LEVEL'] + ' (Margin: ' + newAlarm['LEVEL_MARGIN'] + ')'));
    
    alarms[newAlarm.epic + '-' + newAlarm.alarmType] = newAlarm;
    
    $('#modalAlarm').modal('hide');
    var count = 0;
    
    for (var alarmKey in alarms) {
        if (alarms.hasOwnProperty(alarmKey)) {
            var alarm = alarms[alarmKey];
            count++;
            if(!$('#TOGGLE_ACTIVE_ALARM_' + alarmKey.replace(/\./g, "_")).length){
                $('#alarmList').append('<a class="lv-item" id="TOGGLE_ACTIVE_ALARM_' + alarmKey.replace(/\./g, "_") + '" data-no-pjax>'
                    + '<div class="media">'
                        + '<div class="pull-left">'
                            + '<img class="lv-img-sm" id="IMAGE_ALARM_' + alarmKey.replace(/\./g, "_") + '" src="/img/icons/notification-' + (alarm.active ? 'active' : 'inactive') + '.png" alt="">'
                        + '</div>'
                        + '<div class="media-body">'
                            + '<div class="lv-title">' + alarm.epicName + '</div>'
                            + '<small class="lv-small" id="ALARM_TYPE_' + alarmKey.replace(/\./g, "_") + '">' + alarm.alarmTypeName + '</small>'
                        + '</div>'
                    + '</div>'
                + '</a>');
                switch(alarm.alarmType){
                    case '0_50_LEVEL':
                        $('#ALARM_ICONS_' + alarm.epic.replace(/\./g, "_")).append('<div id="ALARM_BUTTON_GROUP_' + alarmKey.replace(/\./g, "_") + '" class="btn-group m-r-5">'
                                + '<button id="ALARM_BUTTON_' + alarmKey.replace(/\./g, "_") + '" type="button" class="btn btn-default waves-effect' + (!alarm.active ? ' c-red' : (!alarm.playSound ? ' c-red' : '')) + '" data-trigger="hover" data-toggle="popover" data-placement="top" data-content="' + alarm.alarmTypeName + '"><i class="zmdi zmdi-format-align-center"></i></button>'
                                + '<button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">'
                                    + '<span class="caret"></span>'
                                    + '<span class="sr-only">Split button dropdowns</span>'
                                + '</button>'
                                + '<ul class="dropdown-menu" role="menu">'
                                    + '<li><a href="#" id="ALARM_ACTIVE_BUTTON_' + alarmKey.replace(/\./g, "_") + '">' + (alarm.active ? '<i class="zmdi zmdi-alarm-off"></i> Turn Off' : '<i class="zmdi zmdi-alarm-check"></i> Turn On') + '</a></li>'
                                    + '<li><a href="#" id="ALARM_SOUND_BUTTON_' + alarmKey.replace(/\./g, "_") + '">' + (alarm.playSound ? '<i class="zmdi zmdi-notifications-off"></i> Mute' : '<i class="zmdi zmdi-notifications-active"></i> Unmute') + '</a></li>'
                                    + '<li class="divider"></li>'
                                    + '<li><a href="#" id="ALARM_REMOVE_BUTTON_' + alarmKey.replace(/\./g, "_") + '"><i class="zmdi zmdi-delete"></i> Remove</a></li>'
                                + '</ul>'
                            + '</div>');
                        $('#ALARM_BUTTON_' + alarmKey.replace(/\./g, "_")).popover();
                        break;
                        
                    case 'PP_LEVEL':
                        break;
                        
                    default:
                        $('#ALARM_ICONS_' + alarm.epic.replace(/\./g, "_")).append('<div id="ALARM_BUTTON_GROUP_' + alarmKey.replace(/\./g, "_") + '" class="btn-group m-r-5">'
                                + '<button id="ALARM_BUTTON_' + alarmKey.replace(/\./g, "_") + '" type="button" class="btn btn-default waves-effect' + (!alarm.active ? ' c-red' : (!alarm.playSound ? ' c-red' : '')) + '" data-trigger="hover" data-toggle="popover" data-placement="top" data-content="' + alarm.alarmTypeName + '">' + alarm.LEVEL + '</button>'
                                + '<button type="button" class="btn btn-default dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false">'
                                    + '<span class="caret"></span>'
                                    + '<span class="sr-only">Split button dropdowns</span>'
                                + '</button>'
                                + '<ul class="dropdown-menu" role="menu">'
                                    + '<li><a href="#" id="ALARM_ACTIVE_BUTTON_' + alarmKey.replace(/\./g, "_") + '">' + (alarm.active ? '<i class="zmdi zmdi-alarm-off"></i> Turn Off' : '<i class="zmdi zmdi-alarm-check"></i> Turn On') + '</a></li>'
                                    + '<li><a href="#" id="ALARM_SOUND_BUTTON_' + alarmKey.replace(/\./g, "_") + '">' + (alarm.playSound ? '<i class="zmdi zmdi-notifications-off"></i> Mute' : '<i class="zmdi zmdi-notifications-active"></i> Unmute') + '</a></li>'
                                    + '<li class="divider"></li>'
                                    + '<li><a href="#" id="ALARM_REMOVE_BUTTON_' + alarmKey.replace(/\./g, "_") + '"><i class="zmdi zmdi-delete"></i> Remove</a></li>'
                                + '</ul>'
                            + '</div>');
                        $('#ALARM_BUTTON_' + alarmKey.replace(/\./g, "_")).popover();
                        
                }
                $('#ALARM_REMOVE_BUTTON_' + alarmKey.replace(/\./g, "_")).on(clickEventTrigger, function(e){
                    e.preventDefault();
                    var first = $(this).attr('id').split('ALARM_REMOVE_BUTTON_')[1];
                    removeAlarm(first.split(/-(.+)?/)[0].replace(/\_/g, ".") + '-' + first.split(/-(.+)?/)[1]);
                });
                $('#ALARM_ACTIVE_BUTTON_' + alarmKey.replace(/\./g, "_")).on(clickEventTrigger, function(e){
                    e.preventDefault();
                    var first = $(this).attr('id').split('ALARM_ACTIVE_BUTTON_')[1];
                    updateAlarm('active', first.split(/-(.+)?/)[0].replace(/\_/g, ".") + '-' + first.split(/-(.+)?/)[1]);
                });
                $('#ALARM_SOUND_BUTTON_' + alarmKey.replace(/\./g, "_")).on(clickEventTrigger, function(e){
                    e.preventDefault();
                    var first = $(this).attr('id').split('ALARM_SOUND_BUTTON_')[1];
                    updateAlarm('sound', first.split(/-(.+)?/)[0].replace(/\_/g, ".") + '-' + first.split(/-(.+)?/)[1]);
                });
                $('#ALARM_BUTTON_' + alarmKey.replace(/\./g, "_")).on(clickEventTrigger, function(e){
                    e.preventDefault();
                    var first = $(this).attr('id').split('ALARM_BUTTON_')[1];
                    updateAlarm('sound', first.split(/-(.+)?/)[0].replace(/\_/g, ".") + '-' + first.split(/-(.+)?/)[1]);
                });
            }
            else{
                $('#ALARM_TYPE_' + alarmKey.replace(/\./g, "_")).html(alarm.alarmTypeName);
                $('#IMAGE_ALARM_' + alarmKey.replace(/\./g, "_")).attr('src', '/img/icons/notification-' + (alarm.active ? 'active' : 'inactive') + '.png')
            }
        }
    }
    $('#alarmsCount').html(count);
    //console.log(count);
    //console.log(alarms);
    if(count > 0){
        $('#alarmsCount').removeClass('hidden')
    }
    else{
        $('#alarmsCount').addClass('hidden')
    }
    notify('Alarm added / updated!', 'success');
    //console.log(alarms);
}
    
/*
 * Update Alarm
 */
function updateAlarm(type, alarmToUpdate){
    var alarm = alarms[alarmToUpdate];
    switch(type){
        case 'active':
            if(alarm.active){
                alarm.active = false;
                alarm.soundPlayed = false;
                $('#ALARM_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).addClass('c-red');
                $('#ALARM_ACTIVE_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).html('<i class="zmdi zmdi-alarm-check"></i> Turn On');
                $('#IMAGE_ALARM_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).attr('src', '/img/icons/notification-inactive.png');
            }
            else{
                alarm.active = true;
                $('#IMAGE_ALARM_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).attr('src', '/img/icons/notification-active.png');
                $('#ALARM_ACTIVE_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).html('<i class="zmdi zmdi-alarm-off"></i> Turn Off');
                if(alarm.playSound){
                    $('#ALARM_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).removeClass('c-red');
                }
                else{
                    $('#ALARM_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).addClass('c-red');
                }
            }
            break;
            
        case 'sound':
            if(alarm.playSound){
                alarm.playSound = false;
                alarm.soundPlayed = false;
                $('#ALARM_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).addClass('c-red');
                $('#ALARM_SOUND_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).html('<i class="zmdi zmdi-notifications-active"></i> Unmute');
            }
            else{
                alarm.playSound = true;
                $('#ALARM_SOUND_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).html('<i class="zmdi zmdi-notifications-off"></i> Mute');
                if(alarm.active){
                    $('#ALARM_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).removeClass('c-red');
                }
                else{
                    $('#ALARM_BUTTON_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).addClass('c-red');
                }
            }
            break;
    }
}  
    
/*
 * Remove Alarm
 */
function removeAlarm(alarmToRemove){
    var alarm = alarms[alarmToRemove];
    $('#ALARM_BUTTON_GROUP_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).remove();
    $('#TOGGLE_ACTIVE_ALARM_' + alarm.epic.replace(/\./g, "_") + '-' + alarm.alarmType).remove();
    var alarmsCount = +$('#alarmsCount').html();
    alarmsCount--;
    if(alarmsCount <= 0){
        alarmsCount = 0;
        $('#alarmsCount').addClass('hidden').html(alarmsCount);
    }
    else{
        $('#alarmsCount').removeClass('hidden').html(alarmsCount);
    }
    delete alarms[alarmToRemove];
}
    
/*
 * Run alarms
 */
function runAlarms(epic, bid, offer){
    for (var alarm in alarms){
        var alarmData = alarms[alarm];
        var hasToBlink = false;
        if(alarmData.epic == epic){
            if(alarmData.active){
                var type = alarm.split("-")[1];
                var middlePrice = ((+bid + +offer)/2).toString().split('.')[0];
                var res = middlePrice.substring(middlePrice.length-2, middlePrice.length);
                switch(type){
                    case "0_50_LEVEL":
                        if((res >= 50-alarmData['0_50_LEVEL_MARGIN'] && res <= 50+alarmData['0_50_LEVEL_MARGIN']) || 
                           (res >= 100-alarmData['0_50_LEVEL_MARGIN'] || res <= 0+alarmData['0_50_LEVEL_MARGIN'])){
                            hasToBlink = true;
                            if(alarmData.playSound && !alarmData.soundPlayed){
                                mySound.play();
                                alarmData.soundPlayed = true;
                            }
                        }
                        else{
                            hasToBlink = false;
                            alarmData.soundPlayed = false;
                        }
                        break;

                    case "LEVEL":
                        if(middlePrice >= alarmData.LEVEL-alarmData.LEVEL_MARGIN && middlePrice <= alarmData.LEVEL+alarmData.LEVEL_MARGIN){
                            hasToBlink = true;
                            if(alarmData.playSound && !alarmData.soundPlayed){
                                mySound.play();
                                alarmData.soundPlayed = true;
                            }
                        }
                        else{
                            hasToBlink = false;
                            alarmData.soundPlayed = false;
                        }
                        break;

                    case "PP_LEVEL":
                        hasToBlink = false;
                        break;
                        
                    default:
                        hasToBlink = false;
                        alarmData.soundPlayed = false;
                }
            }
            else{
                hasToBlink = false;
                alarmData.soundPlayed = false;
            }
            if(hasToBlink){
                $('#ALARM_BUTTON_' + alarm.replace(/\./g, "_")).addClass('blink bgm-lime');
                
            }
            else{
                $('#ALARM_BUTTON_' + alarm.replace(/\./g, "_")).removeClass('blink bgm-lime');
            }
        }
    }
}
    
/*
 * Update a single trade, SL to 0 or LL to 0
 */
function updateTrade(dealId, updateType){
     // Create a PUT request to /positions/otc/{dealId}
   var req = new Request();
   req.method = "PUT";
   req.url = urlRoot + "/positions/otc/";
    
   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "Version": "1"
   };
    
    switch(updateType){
        case "SL_TO_0":
            var openLevel = $('#OPEN_PRICE_' + dealId).html();
            // Set up the request body
           var bodyParams = {};
           bodyParams["stopLevel"] = openLevel;

           req.body = JSON.stringify(bodyParams);

           // Send the request via a Javascript AJAX call
           var resultData;
           try {
              $.ajax({
                 type: req.method,
                 url: req.url + dealId,
                 data: req.body,
                 headers: req.headers,
                 async: true,
                 mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                 error: function (response, status, error) {
                    // Something went wrong
                    handleHTTPError(response);
                 },
                 success: function (response, status, data) {
                    // The order was deleted
                    //console.log("order updated");
                    resultData = response;
                 }
              });
           } catch (e) {
              handleException(e);
           }
            break;
            
        case "LL_TO_0":
            var openLevel = $('#OPEN_PRICE_' + dealId).html();
            // Set up the request body
           var bodyParams = {};
           bodyParams["limitLevel"] = openLevel;

           req.body = JSON.stringify(bodyParams);

           // Send the request via a Javascript AJAX call
           var resultData;
           try {
              $.ajax({
                 type: req.method,
                 url: req.url + dealId,
                 data: req.body,
                 headers: req.headers,
                 async: true,
                 mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                 error: function (response, status, error) {
                    // Something went wrong
                    handleHTTPError(response);
                 },
                 success: function (response, status, data) {
                    // The order was deleted
                    //console.log("order updated");
                    resultData = response;
                 }
              });
           } catch (e) {
              handleException(e);
           }
            break;
    }
}
    
/*
 * Update trades, SL to 0 or LL to 0
 */
function updateTrades(epic, updateType){
     // Create a PUT request to /positions/otc/{dealId}
   var req = new Request();
   req.method = "PUT";
   req.url = urlRoot + "/positions/otc/";
    
   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "Version": "1"
   };
    
    switch(updateType){
        case "SL_TO_0":
            $('#OPEN_TRADES_' + epic.replace(/\./g, "_") + ' tr').each(function(){
                var dealId = $(this).attr('id');
                var openLevel = $('#OPEN_PRICE_' + dealId).html();
                // Set up the request body
               var bodyParams = {};
               bodyParams["stopLevel"] = openLevel;

               req.body = JSON.stringify(bodyParams);

               // Send the request via a Javascript AJAX call
               var resultData;
               try {
                  $.ajax({
                     type: req.method,
                     url: req.url + dealId,
                     data: req.body,
                     headers: req.headers,
                     async: true,
                     mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                     error: function (response, status, error) {
                        // Something went wrong
                        handleHTTPError(response);
                     },
                     success: function (response, status, data) {
                        // The order was deleted
                        //console.log("order updated");
                        resultData = response;
                     },
                    complete : function(resultat, statut){
                        responseTimeSending = new Date();
                    }
                  });
               } catch (e) {
                  handleException(e);
               }
            });
            break;
            
        case "SL_TO_PRU":
            $('#OPEN_TRADES_' + epic.replace(/\./g, "_") + ' tr').each(function(){
                var dealId = $(this).attr('id');
                var stopLevel = $('#PRU_' + epic.replace(/\./g, "_")).html();
                // Set up the request body
               var bodyParams = {};
               bodyParams["stopLevel"] = stopLevel;

               req.body = JSON.stringify(bodyParams);

               // Send the request via a Javascript AJAX call
               var resultData;
               try {
                  $.ajax({
                     type: req.method,
                     url: req.url + dealId,
                     data: req.body,
                     headers: req.headers,
                     async: true,
                     mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                     error: function (response, status, error) {
                        // Something went wrong
                        handleHTTPError(response);
                     },
                     success: function (response, status, data) {
                        // The order was deleted
                        //console.log("order updated");
                        resultData = response;
                     },
                      complete : function(resultat, statut){
                          responseTimeSending = new Date();
                      }
                  });
               } catch (e) {
                  handleException(e);
               }
            });
            break;
            
        case "LL_TO_0":
            $('#OPEN_TRADES_' + epic.replace(/\./g, "_") + ' tr').each(function(){
                var dealId = $(this).attr('id');
                var openLevel = $('#OPEN_PRICE_' + dealId).html();
                // Set up the request body
               var bodyParams = {};
               bodyParams["limitLevel"] = openLevel;

               req.body = JSON.stringify(bodyParams);

               // Send the request via a Javascript AJAX call
               var resultData;
               try {
                  $.ajax({
                     type: req.method,
                     url: req.url + dealId,
                     data: req.body,
                     headers: req.headers,
                     async: true,
                     mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                     error: function (response, status, error) {
                        // Something went wrong
                        handleHTTPError(response);
                     },
                     success: function (response, status, data) {
                        // The order was deleted
                        //console.log("order updated");
                        resultData = response;
                     },
                      complete : function(resultat, statut){
                          responseTimeSending = new Date();
                      }
                  });
               } catch (e) {
                  handleException(e);
               }
            });
            break;
    }
}

    /*
     * Retrieve market details
     */
    function marketDetails(epic) {

        // Set up the request as a GET request to the address /markets with a path parameter of the market EPIC
        var req = new Request();
        req.method = "GET";
        req.url = urlRoot + "/markets/" + epic;

        // Set up the request headers, i.e. the api key, the account security session token, the client security token,
        // the request content type (JSON), and the expected response content type (JSON)
        req.headers = {
            "X-IG-API-KEY": apiKey,
            "X-SECURITY-TOKEN": account_token,
            "CST": client_token,
            "Content-Type": "application/json; charset=UTF-8",
            "Accept": "application/json; charset=UTF-8",
            "Version": 3
        };

        // No body is required, as this is a GET request
        req.body = "";
        //$("#request_data").text(js_beautify(req.body) || "");

        // Send the request via a Javascript AJAX call
        var resultData;
        try {
            $.ajax({
                type: req.method,
                url: req.url,
                data: req.body,
                headers: req.headers,
                async: false,
                mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                error: function (response, status, error) {
                    // Something went wrong
                    handleHTTPError(response);
                },
                success: function (response, status, data) {

                    // Market details were returned
                    // Prettify the response and store the result for later display
                    //$("#response_data").text(js_beautify(data.responseText) || "");
                    //console.log("market details retrieved");
                    //console.log(response);
                    resultData = response;
                }
            });

        } catch (e) {
            handleException(e);
        }
        return resultData;
    }

    /*
    *
    * Update market sentiment on demand
     */
    function updateSentiment(marketId, epic, notification){
        var sentiment = marketSentiment(marketId);
        //console.log(sentiment);
        $('#MARKET_SENTIMENT_BUY_' + epic).width(sentiment.longPositionPercentage + '%');
        $('#MARKET_SENTIMENT_BUY_VALUE_' + epic).html(sentiment.longPositionPercentage + '%');
        $('#MARKET_SENTIMENT_SELL_' + epic).width(sentiment.shortPositionPercentage + '%');
        $('#MARKET_SENTIMENT_SELL_VALUE_' + epic).html(sentiment.shortPositionPercentage + '%');
        if(notification){
            notify('Market sentiment updated!', 'success');
        }
    }

    /*
     * Retrieve market sentiment
     */
    function marketSentiment(marketId) {

        // Set up the request as a GET request to the address /clientsentiment with a path parameter of the market EPIC
        var req = new Request();
        req.method = "GET";
        req.url = urlRoot + "/clientsentiment/" + marketId;

        // Set up the request headers, i.e. the api key, the account security session token, the client security token,
        // the request content type (JSON), and the expected response content type (JSON)
        req.headers = {
            "X-IG-API-KEY": apiKey,
            "X-SECURITY-TOKEN": account_token,
            "CST": client_token,
            "Content-Type": "application/json; charset=UTF-8",
            "Accept": "application/json; charset=UTF-8"
        };

        // No body is required, as this is a GET request
        req.body = "";
        //$("#request_data").text(js_beautify(req.body) || "");

        // Send the request via a Javascript AJAX call
        var resultData;
        try {
            $.ajax({
                type: req.method,
                url: req.url,
                data: req.body,
                headers: req.headers,
                async: false,
                mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                error: function (response, status, error) {
                    // Something went wrong
                    handleHTTPError(response);
                },
                success: function (response, status, data) {

                    // Market details were returned
                    // Prettify the response and store the result for later display
                    //$("#response_data").text(js_beautify(data.responseText) || "");
                    //console.log("market details retrieved");
                    //console.log(response);
                    resultData = response;
                }
            });

        } catch (e) {
            handleException(e);
        }
        //marketSentimentRelated(marketId);
        return resultData;
    }

    function marketSentimentRelated(marketId) {

        // Set up the request as a GET request to the address /clientsentiment with a path parameter of the market EPIC
        var req = new Request();
        req.method = "GET";
        req.url = urlRoot + "/clientsentiment/related/" + marketId;

        // Set up the request headers, i.e. the api key, the account security session token, the client security token,
        // the request content type (JSON), and the expected response content type (JSON)
        req.headers = {
            "X-IG-API-KEY": apiKey,
            "X-SECURITY-TOKEN": account_token,
            "CST": client_token,
            "Content-Type": "application/json; charset=UTF-8",
            "Accept": "application/json; charset=UTF-8"
        };

        // No body is required, as this is a GET request
        req.body = "";
        //$("#request_data").text(js_beautify(req.body) || "");

        // Send the request via a Javascript AJAX call
        var resultData;
        try {
            $.ajax({
                type: req.method,
                url: req.url,
                data: req.body,
                headers: req.headers,
                async: false,
                mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
                error: function (response, status, error) {
                    // Something went wrong
                    handleHTTPError(response);
                },
                success: function (response, status, data) {

                    // Market details were returned
                    // Prettify the response and store the result for later display
                    //$("#response_data").text(js_beautify(data.responseText) || "");
                    //console.log("market details retrieved");
                    console.log(response);
                    resultData = response;
                }
            });

        } catch (e) {
            handleException(e);
        }
        return resultData;
    }

/*
 * Function to populate the deal trading ticket
 */
function dealTicket(epic, expiry) {

   // Unsubscribe from any previous Lightstreamer subscriptions
   if (ticketSubscription) {
      lsClient.unsubscribe(ticketSubscription);
   }

   // Get the EPIC of the market we want to trade against
   $('#trade_epic').val(epic);
   $('#trade_expiry').val(expiry);
   var market = marketDetails(epic);
   $('#dealTicket_title').text('Deal Ticket - ' + market.instrument.name);

   // Set the buy and sell
   $('#ticket_buy_price').text(market.snapshot.offer);
   $('#ticket_sell_price').text(market.snapshot.bid);

   $('#trade_offer').val(market.snapshot.offer);
   $('#trade_bid').val(market.snapshot.bid);

   // Create a Lightstreamer subscription for the BID and OFFER prices for the relevant market

      // Set up the Lightstreamer FIDs
      ticketSubscription = new Subscription(
         "MERGE",
         "L1:" + epic,
         [
            "BID",
            "OFFER"
         ]
      );

     ticketSubscription.setRequestedSnapshot("yes");

      // Set up the Lightstreamer event listeners
      ticketSubscription.addListener({
         onSubscription: function () {
            //console.log('subscribed');
         },
         onSubscriptionError: function (code, message) {
            //console.log('subscription failure: ' + code + " message: " + message);
         },
         onItemUpdate: function (updateInfo) {

            // Lightstreamer notification received
            // Extract the BID and OFFER prices and display these
            var epic = updateInfo.getItemName().split("|")[1];
            var tidyEpic = epic.replace(/\./g, "_");
            updateInfo.forEachField(function (fieldName, fieldPos, value) {
               if (fieldName == "BID") {
                  $('#ticket_sell_price').text(value);
                  $('#trade_bid').text(value);
               } else if (fieldName == "OFFER") {
                  $('#ticket_buy_price').text(value);
                  $('#trade_offer').text(value);
               }
            });
         }
      });

      // Subscribe to Lightstreamer
      lsClient.subscribe(ticketSubscription);

   // Show deal ticket
   $('#dealTicket').modal('show');
}

    
/*
 * Function to close all OTC positions of a given epic
 */
function closeAllEpicTrades(epic){
    // Create a DELETE request to /positions/otc
   var req = new Request();
   req.method = "POST";
   req.url = urlRoot + "/positions/otc";
    
   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "_method": "DELETE",
      "Version": "1"
   };
    
    $('#OPEN_TRADES_' + epic.replace(/\./g, "_") + ' tr').each(function(){
        var dealId = $(this).attr('id');
        var size = $('#SIZE_' + dealId).html();
        // Set up the request body
       var bodyParams = {};
       bodyParams["dealId"] = dealId;
       /*bodyParams["epic"] = $('#' + dealId).parent().attr('id').split("OPEN_TRADES_")[1].replace(/\_/g, ".");
       bodyParams["expiry"] = "-";*/
       bodyParams["direction"] = (size < 0 ? "BUY" : "SELL");
       bodyParams["size"] = (size < 0 ? size*-1 : size);
       bodyParams["orderType"] = "MARKET";


        //console.log(bodyParams);

       req.body = JSON.stringify(bodyParams);

       // Send the request via a Javascript AJAX call
       var resultData;
       try {
          $.ajax({
             type: req.method,
             url: req.url,
             data: req.body,
             headers: req.headers,
             async: false,
             mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
             error: function (response, status, error) {
                // Something went wrong
                handleHTTPError(response);
             },
             success: function (response, status, data) {
                // The order was deleted
                //console.log("order deleted");
                resultData = response;
             },
              complete : function(resultat, statut){
                  responseTimeSending = new Date();
              }
          });
       } catch (e) {
          handleException(e);
       }

       // If the deal was placed, wait for the deal confirmation
       if (resultData) {
          showDealInProgressDialog(resultData);
       }
    });
}
        
/*
 * Function to close all OTC positions regardless the epix
 */
function closeAllTrades(){
    // Create a DELETE request to /positions/otc
   var req = new Request();
   req.method = "POST";
   req.url = urlRoot + "/positions/otc";
    
   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "_method": "DELETE",
      "Version": "1"
   };
    
    for(var dealId in openTrades){
        var size = $('#SIZE_' + dealId).html();
        // Set up the request body
       var bodyParams = {};
       bodyParams["dealId"] = dealId;
       /*bodyParams["epic"] = $('#' + dealId).parent().attr('id').split("OPEN_TRADES_")[1].replace(/\_/g, ".");
       bodyParams["expiry"] = "-";*/
       bodyParams["direction"] = (size < 0 ? "BUY" : "SELL");
       bodyParams["size"] = (size < 0 ? size*-1 : size);
       bodyParams["orderType"] = "MARKET";


        //console.log(bodyParams);

       req.body = JSON.stringify(bodyParams);

       // Send the request via a Javascript AJAX call
       var resultData;
       try {
          $.ajax({
             type: req.method,
             url: req.url,
             data: req.body,
             headers: req.headers,
             async: false,
             mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
             error: function (response, status, error) {
                // Something went wrong
                handleHTTPError(response);
             },
             success: function (response, status, data) {
                // The order was deleted
                //console.log("order deleted");
                resultData = response;
             }
          });
       } catch (e) {
          handleException(e);
       }

       // If the deal was placed, wait for the deal confirmation
       if (resultData) {
          showDealInProgressDialog(resultData);
       }
    }
}
    
/*
 * Function to close an OTC position
 */
function closeTrade(dealId, size){
   // Create a DELETE request to /positions/otc
   var req = new Request();
   req.method = "POST";
   req.url = urlRoot + "/positions/otc";
    
   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "_method": "DELETE",
      "Version": "1"
   };

   // Set up the request body
   var bodyParams = {};
   bodyParams["dealId"] = dealId;
   /*bodyParams["epic"] = $('#' + dealId).parent().attr('id').split("OPEN_TRADES_")[1].replace(/\_/g, ".");
   bodyParams["expiry"] = "-";*/
   bodyParams["direction"] = (size < 0 ? "BUY" : "SELL");
   bodyParams["size"] = (size < 0 ? size*-1 : size);
   bodyParams["orderType"] = "MARKET";
    

    //console.log(bodyParams);
    
   req.body = JSON.stringify(bodyParams);

   // Send the request via a Javascript AJAX call
   var resultData;
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         data: req.body,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
            // Something went wrong
            handleHTTPError(response);
         },
         success: function (response, status, data) {
            // The order was deleted
            //console.log("order deleted");
            resultData = response;
         },
          complete : function(resultat, statut){
              responseTimeSending = new Date();
          }
      });
   } catch (e) {
      handleException(e);
   }

   // If the deal was placed, wait for the deal confirmation
   if (resultData) {
      showDealInProgressDialog(resultData);
   }
}

/*
 * Function to create an OTC position
 */
function placeTrade(epic, expiry, size, tradeBid, tradeOffer, direction, forceOpen, guaranteedStop, stopLevel, limitLevel, currency) {
   // Create a POST request to /positions/otc
    //console.log(epic, expiry, size, tradeBid, tradeOffer, direction, forceOpen, guaranteedStop, stopLevel, limitLevel, currency);
   var req = new Request();
   req.method = "POST";
   req.url = urlRoot + "/positions/otc";

   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "Version": "1"
   };

   // Set up the request body
   var bodyParams = {};
   bodyParams["currencyCode"] = currency;
   bodyParams["epic"] = epic;
   bodyParams["expiry"] = expiry;
   bodyParams["direction"] = direction;
   bodyParams["size"] = size;
   bodyParams["forceOpen"] = forceOpen;
   bodyParams["guaranteedStop"] = guaranteedStop;
   //bodyParams["level"] = (direction == "BUY" ? tradeOffer : tradeBid);
   bodyParams["stopLevel"] = (stopLevel > 0 ? (direction == "BUY" ? Number(+tradeOffer - +stopLevel) : Number(+tradeBid + +stopLevel)) : null);
   bodyParams["limitLevel"] = (limitLevel > 0 ? (direction == "BUY" ? Number(+tradeOffer + +limitLevel) : Number(+tradeBid - +limitLevel)) : null);
   bodyParams["orderType"] = "MARKET";

   req.body = JSON.stringify(bodyParams);

   // Prettify the request for display purposes only
   //$("#request_data").text(js_beautify(req.body) || "");

   // Send the request via a Javascript AJAX call
   var resultData;
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         data: req.body,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
            // Something went wrong
            handleHTTPError(response);
         },
         success: function (response, status, data) {
            // The order was created
            // Prettify and log the response
            //$("#response_data").text(js_beautify(data.responseText) || "");
            //console.log("order placed");
            resultData = response;
         },
          complete : function(resultat, statut){
              responseTimeSending = new Date();
          }
      });
   } catch (e) {
      handleException(e);
   }

   // If the deal was placed, wait for the deal confirmation
   if (resultData) {
      showDealInProgressDialog(resultData);
   }

//   Note: this example relies on the Lightstreamer confirm, alternatively a client implementation might make use of the polling confirm service, illustrated below
//   setTimeout(function () {
//      var message = retrieveConfirm(resultData.dealReference);
//      //console.log(message);
//      $('#dealInProgress').modal("hide");
//      showDealConfirmDialog(message);
//   }, 1000);

}

/*
 * Function to display an in progress dialog for an order while we wait for its confirmation.
 */
function showDealInProgressDialog(resultData) {
    //console.log(resultData);
}


/*
 * Function to display the deal confirmation message and update the user interface positions list
 */
function showDealConfirmDialog(message) {
    responseTimeWaiting = new Date();
   if (message) {
       message = jQuery.parseJSON(message);
        //console.log("Confirm received!!!");
        //console.log(message);
        if(message.status === "OPEN" && message.reason === "SUCCESS"){
            $('#OPEN_TRADES_' + message.epic.replace(/\./g, "_")).append('<tr class="" id="' + message.dealId + '">'
                + '<td><i class="zmdi zmdi-long-arrow-' + (message.direction == "BUY" ? "up" : "down") + '"></i></td>'
                + '<td><span id="SIZE_' + message.dealId + '">' + (message.direction == "BUY" ? message.size : message.size*-1) + '</span></td>'
                + '<td>OL: <span id="OPEN_PRICE_' + message.dealId + '">' + message.level + '</span><br/><small>SL: <span id="STOP_LEVEL_' + message.dealId + '">' + (message.stopLevel ? message.stopLevel : "-") + '</span><br/>LL: <span id="LIMIT_LEVEL_' + message.dealId + '">' + (message.limitLevel ? message.limitLevel : "-") + '</span></small></td>'
                + '<td><span class="f-17" id="POINTS_' + message.dealId + '"></span></td>'
            + '</tr>');
            
            openTrades[message.dealId] = 0;
            //console.log(openTrades);
            
            $('#' + message.dealId).on(clickEventTrigger, function(e){
                if(e.which == 1){
                    responseTimeStart = new Date();
                    closeTrade($(this).attr('id'), $('#SIZE_' + $(this).attr('id')).html());
                }
            });
            $.contextMenu({
                selector: '#' + message.dealId, 
                callback: function(key, options) {
                    //console.log(options);
                    switch(key){
                        case 'close':
                            closeTrade($(options.selector).attr('id'), $('#SIZE_' + $(options.selector).attr('id')).html());
                            break;
                            
                        case 'llto0':
                            updateTrade($(options.selector).attr('id'), "LL_TO_0");
                            break;
                            
                        case 'slto0':
                            updateTrade($(options.selector).attr('id'), "SL_TO_0");
                            break;
                    }
                },
                items: {
                    "slto0": {name: '<i id="connection-icon" class="zmdi zmdi-download"></i> SL to 0'},
                    "llto0": {name: '<i id="connection-icon" class="zmdi zmdi-upload"></i> LL to 0'},
                    "sep1": "---------",
                    "close": {name: '<i id="connection-icon" class="zmdi zmdi-close"></i> Close'}
                }
            });
            
        }
        if(message.status === "CLOSED" && message.reason === "SUCCESS"){
            $('#' + message.dealId).remove();
            delete openTrades[message.dealId];
            if($.isEmptyObject(openTrades)){
                $('#PNLCP').html(0);
                $('#PNLCPColor').removeClass('c-deeporange').removeClass('c-lightblue');
            }
            updateDailyPnl();
            populateHistoryTable();
            updateStatistics($('#FROM_STATS').html(), $('#TO_STATS').html());
        }
        if(message.status === "DELETED"){
            $('#' + message.dealId).remove();
            delete openTrades[message.dealId];
            if($.isEmptyObject(openTrades)){
                $('#PNLCP').html(0);
                $('#PNLCPColor').removeClass('c-deeporange').removeClass('c-lightblue');
            }
            updateDailyPnl();
            populateHistoryTable();
            updateStatistics($('#FROM_STATS').html(), $('#TO_STATS').html());
        }
        if(message.status === "UPDATED"){
            $('#SIZE_' + message.dealId).html((message.direction == "BUY" ? message.size : message.size*-1));
            $('#OPEN_PRICE_' + message.dealId).html(message.level);
            $('#STOP_LEVEL_' + message.dealId).html((message.stopLevel ? message.stopLevel : "-"));
            $('#LIMIT_LEVEL_' + message.dealId).html((message.limitLevel ? message.limitLevel : "-"));
        }
        if(message.dealStatus === "REJECTED"){
            handleTradeError('Deal #' + message.dealId + ' rejected!', message.reason);
        }
        updatePru(message.epic.replace(/\./g, "_"));

        responseTimeStop = new Date();
        if(responseTimeStart !== null){
            var grt = responseTimeStop - responseTimeStart;
            var st = responseTimeSending - responseTimeStart;
            var wt = responseTimeWaiting - responseTimeSending;
            var rt = responseTimeStop - responseTimeWaiting;
            var swt = st + wt;
            //console.log(responseTimeStart);
            //console.log(responseTimeSending);
            //console.log(responseTimeWaiting);
            //console.log(responseTimeStop);
            //console.log('Global:' + grt + 'ms / Sending:' + st + 'ms / Waiting:' + wt + 'ms / Treatment:' + rt + 'ms');
            $('#rt-text').html('Global:' + grt + 'ms / Sending:' + st + 'ms / Waiting:' + wt + 'ms / Treatment:' + rt + 'ms');
            var notifyStyle = 'info';
            if(swt <= 150){
                $('#rt-li-a').addClass('bgm-green').removeClass('bgm-red').removeClass('bgm-orange');
                notifyStyle = 'success';
            }
            else{
                if(swt >= 500){
                    $('#rt-li-a').addClass('bgm-red').removeClass('bgm-green').removeClass('bgm-orange');
                    notifyStyle = 'danger';
                }
                else{
                    $('#rt-li-a').addClass('bgm-orange').removeClass('bgm-green').removeClass('bgm-red');
                    notifyStyle = 'warning';
                }
            }
            notify('Global:' + grt + 'ms / Sending:' + st + 'ms / Waiting:' + wt + 'ms / Treatment:' + rt + 'ms', notifyStyle);
            responseTimeStart = responseTimeSending = responseTimeWaiting = responseTimeStop = null;
        }
        //positions();
   }

}
    
function updatePru(epic){
    var price = 0;
    var lots = 0;
    $('#OPEN_TRADES_' + epic + ' tr').each(function(){
        var deal = $(this).attr('id');
        price += +$('#OPEN_PRICE_' + deal).html() * Math.abs(+$('#SIZE_' + deal).html());
        lots += Math.abs(+$('#SIZE_' + deal).html());
    });
    var pru = (price / lots).toFixed(1);
    //console.log(pru);
    $('#PRU_' + epic.replace(/\./g, "_")).html(pru);
}
    
function populatePositionsOnSubscription(response){
    
    $(response.positions).each(function (index) {
       var positionData = response.positions[index];
       var epic = positionData.market.epic;
       var canSubscribe = positionData.market.streamingPricesAvailable;
       var tidyEpic = epic.replace(/\./g, "_");
        
        var checkExist = setInterval(function() {
           if ($('#OPEN_TRADES_' + tidyEpic).length) {
              clearInterval(checkExist);
               //console.log('exists + ' + positionData.position.openLevel);
               //console.log(positionData);
               if(!$('#' + positionData.position.dealId).length){
                   $('#OPEN_TRADES_' + tidyEpic).append('<tr class="" id="' + positionData.position.dealId + '">'
                        + '<td><i class="zmdi zmdi-long-arrow-' + (positionData.position.direction == "BUY" ? "up" : "down") + '"></i></td>'
                        + '<td><span id="SIZE_' + positionData.position.dealId + '">' + (positionData.position.direction == "BUY" ? positionData.position.dealSize : positionData.position.dealSize*-1) + '</span></td>'
                        + '<td>OL: <span id="OPEN_PRICE_' + positionData.position.dealId + '">' + positionData.position.openLevel + '</span><br/><small>SL: <span id="STOP_LEVEL_' + positionData.position.dealId + '">' + (positionData.position.stopLevel ? positionData.position.stopLevel : "-") + '</span><br/>LL: <span id="LIMIT_LEVEL_' + positionData.position.dealId + '">' + (positionData.position.limitLevel ? positionData.position.limitLevel : "-") + '</span></small></td>'
                        + '<td><span class="f-17" id="POINTS_' + positionData.position.dealId + '"></span></td>'
                    + '</tr>');

                    $('#' + positionData.position.dealId).on(clickEventTrigger, function(e){
                        if(e.which == 1){
                            responseTimeStart = new Date();
                            closeTrade($(this).attr('id'), $('#SIZE_' + $(this).attr('id')).html());
                        }
                    });
                   $.contextMenu({
                        selector: '#' + positionData.position.dealId, 
                        callback: function(key, options) {
                            //console.log(options);
                            switch(key){
                                case 'close':
                                    closeTrade($(options.selector).attr('id'), $('#SIZE_' + $(options.selector).attr('id')).html());
                                    break;

                                case 'llto0':
                                    updateTrade($(options.selector).attr('id'), "LL_TO_0");
                                    break;

                                case 'slto0':
                                    updateTrade($(options.selector).attr('id'), "SL_TO_0");
                                    break;
                            }
                        },
                        items: {
                            "slto0": {name: '<i id="connection-icon" class="zmdi zmdi-download"></i> SL to 0'},
                            "llto0": {name: '<i id="connection-icon" class="zmdi zmdi-upload"></i> LL to 0'},
                            "sep1": "---------",
                            "close": {name: '<i id="connection-icon" class="zmdi zmdi-close"></i> Close'}
                        }
                   });
               }
           }
        updatePru(tidyEpic);
        }, 100);
    });
}
   
/*
 * Function to display the deal confirmation message and update the user interface positions list on login
 */
function populatePositionsOnLogin(response) {
    var epicsItems = [];
    $(response.positions).each(function (index) {
       var positionData = response.positions[index];
       var epic = positionData.market.epic;
       var canSubscribe = positionData.market.streamingPricesAvailable;
       var tidyEpic = epic.replace(/\./g, "_");

       if (canSubscribe) {
          var epicsItem = "L1:" + positionData.market.epic;
           var result = false;
           if(jQuery.inArray(epicsItem, epicsItems) === -1){
               epicsItems.push(epicsItem);
           }
       }
    });
    
    $(epicsItems).each(function(index){
        subscription(epicsItems[index]);
    });
    
    var epicsItems = [];
    $(response.positions).each(function (index) {
       var positionData = response.positions[index];
       var epic = positionData.market.epic;
       var canSubscribe = positionData.market.streamingPricesAvailable;
       var tidyEpic = epic.replace(/\./g, "_");
        
        var checkExist = setInterval(function() {
           if ($('#OPEN_TRADES_' + tidyEpic).length) {
              clearInterval(checkExist);
               //console.log('exists + ' + positionData.position.openLevel);
               //console.log(positionData);
               if(!$('#' + positionData.position.dealId).length){
                   $('#OPEN_TRADES_' + tidyEpic).append('<tr class="" id="' + positionData.position.dealId + '">'
                        + '<td><i class="zmdi zmdi-long-arrow-' + (positionData.position.direction == "BUY" ? "up" : "down") + '"></i></td>'
                        + '<td><span id="SIZE_' + positionData.position.dealId + '">' + (positionData.position.direction == "BUY" ? positionData.position.dealSize : positionData.position.dealSize*-1) + '</span></td>'
                        + '<td>OL: <span id="OPEN_PRICE_' + positionData.position.dealId + '">' + positionData.position.openLevel + '</span><br/><small>SL: <span id="STOP_LEVEL_' + positionData.position.dealId + '">' + (positionData.position.stopLevel ? positionData.position.stopLevel : "-") + '</span><br/>LL: <span id="LIMIT_LEVEL_' + positionData.position.dealId + '">' + (positionData.position.limitLevel ? positionData.position.limitLevel : "-") + '</span></small></td>'
                        + '<td><span class="f-17" id="POINTS_' + positionData.position.dealId + '"></span></td>'
                    + '</tr>');

                    $('#' + positionData.position.dealId).on(clickEventTrigger, function(e){
                        if(e.which == 1){
                            responseTimeStart = new Date();
                            closeTrade($(this).attr('id'), $('#SIZE_' + $(this).attr('id')).html());
                        }
                    });
                    $.contextMenu({
                        selector: '#' + positionData.position.dealId, 
                        callback: function(key, options) {
                            //console.log(options);
                            switch(key){
                                case 'close':
                                    closeTrade($(options.selector).attr('id'), $('#SIZE_' + $(options.selector).attr('id')).html());
                                    break;

                                case 'llto0':
                                    updateTrade($(options.selector).attr('id'), "LL_TO_0");
                                    break;

                                case 'slto0':
                                    updateTrade($(options.selector).attr('id'), "SL_TO_0");
                                    break;
                            }
                        },
                        items: {
                            "slto0": {name: '<i id="connection-icon" class="zmdi zmdi-download"></i> SL to 0'},
                            "llto0": {name: '<i id="connection-icon" class="zmdi zmdi-upload"></i> LL to 0'},
                            "sep1": "---------",
                            "close": {name: '<i id="connection-icon" class="zmdi zmdi-close"></i> Close'}
                        }
                   });
               }
           }
        updatePru(tidyEpic);
        }, 100);
    });

}
    
/*
 * Update the daily PNL
 */
function updateDailyPnl(){
    retrieveTransactions();
    //console.log(retrievedTransactions);
    var dailyPnl = 0;
    var dailyPnlp = 0;
    
    for(var i = 0; i < retrievedTransactions.length; i++){
        if(retrievedTransactions[i].transactionType == "ORDRE"){
            
            dailyPnl += +retrievedTransactions[i].profitAndLoss.split(retrievedTransactions[i].currency)[1];
            dailyPnlp += +(retrievedTransactions[i].size.indexOf("+") > -1 ? (Math.round((retrievedTransactions[i].closeLevel - retrievedTransactions[i].openLevel)*100) / 100) : (Math.round((retrievedTransactions[i].openLevel - retrievedTransactions[i].closeLevel)*100) / 100));
        }
    }
    
    dailyPnl = Math.round(dailyPnl*100) / 100;
    dailyPnlp = Math.round(dailyPnlp*100) / 100;
    
    $('#PNLD').html(dailyPnl);
    $('#PNLP').html(dailyPnlp);
    
    //console.log(dailyPnl + '/' + dailyPnlp);
    if(dailyPnl > 0){
         $('#PNLDColor').removeClass('c-deeporange').addClass('c-lightblue');
     }
     else if(dailyPnl == 0){
         $('#PNLDColor').removeClass('c-deeporange').removeClass('c-lightblue');
     }
     else {
         $('#PNLDColor').addClass('c-deeporange').removeClass('c-lightblue');
     }
    if(dailyPnlp > 0){
         $('#PNLDPColor').removeClass('c-deeporange').addClass('c-lightblue');
     }
     else if(dailyPnlp == 0){
         $('#PNLDPColor').removeClass('c-deeporange').removeClass('c-lightblue');
     }
     else {
         $('#PNLDPColor').addClass('c-deeporange').removeClass('c-lightblue');
     }
}
    
/*
 * Update statistics
 */
function updateStatistics(fromDateTaken, toDateTaken){
    
        var fromDate = new Date(fromDateTaken.split('-')[2], fromDateTaken.split('-')[1], fromDateTaken.split('-')[0]);
        var toDate = new Date(toDateTaken.split('-')[2], toDateTaken.split('-')[1], toDateTaken.split('-')[0]);
        transactionsForStatistics = retrievedTransactions;
   
        var req = new Request();
       req.method = "GET";
       req.url = urlRoot + "/history/transactions?type=ALL&from=" + fromDateTaken.split('-')[2] + "-" + fromDateTaken.split('-')[1] + "-" + fromDateTaken.split('-')[0] + "T00%3A00%3A00&to=" + toDateTaken.split('-')[2] + "-" + toDateTaken.split('-')[1] + "-" + toDateTaken.split('-')[0] + "T23%3A59%3A59&pageSize=0";

       // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
       // the request content type (JSON), and the expected response content type (JSON)      
       req.headers = {
          "X-IG-API-KEY": apiKey,
          "X-SECURITY-TOKEN": account_token,
          "CST": client_token,
          "Content-Type": "application/json; charset=UTF-8",
          "Accept": "application/json; charset=UTF-8",
          "Version": "2"
       };

       // Send the request via a Javascript AJAX call
       try {
          $.ajax({
             type: req.method,
             url: req.url,
             headers: req.headers,
             async: false,
             mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
             error: function (response, status, error) {
                // Something went wrong
                handleHTTPError(response);
             },
             success: function (response, status, data) {
                 //console.log(response);
                 transactionsForStatistics = response.transactions;
             }
          });
       } catch (e) {
          handleException(e);
       }
    
    var pts_won = 0;
    var pts_flat = 0;
    var pts_lost = 0;
    var money_won = 0;
    var money_lost = 0;
    var trade_won = 0;
    var trade_lost = 0;
    var cumulated_points = 0;
    var cumulated_money = 0;
    var chart_points = [[0,0]];
    var chart_money = [[0,0]];
    var higher_high_pts = 0;
    var higher_high_pts_trade = 0;
    var lower_low_pts = 0;
    var lower_low_pts_trade = 0;
    var drawdown_pts = 0;
    var dd_high_pts = 0;
    var dd_low_pts = 0;
    var i = 1;
    $('#STATS_TRADES_LIST').empty();
    if(transactionsForStatistics.length > 0){
        transactionsForStatistics.reverse();
        //console.log(transactionsForStatistics.length);
        //console.log(transactionsForStatistics);
        for(var j = 0; j < transactionsForStatistics.length; j++){
            var trade = transactionsForStatistics[j];
            //console.log(j);
            if(trade.transactionType === 'ORDRE'){
                //console.log(trade);
                var points = trade.size.indexOf("+") > -1 ? (Math.round((trade.closeLevel - trade.openLevel) * 100) / 100) : (Math.round((trade.openLevel - trade.closeLevel) * 100) / 100);
                if(points >= 0){
                    pts_won += +points;
                    trade_won++;
                }
                else{
                    pts_lost += +points;
                    trade_lost++;
                }
                cumulated_points += +points;

                if(cumulated_points > higher_high_pts){
                    higher_high_pts = lower_low_pts = cumulated_points;
                    higher_high_pts_trade = lower_low_pts_trade = j + 1;
                }
                if(cumulated_points < lower_low_pts){
                    lower_low_pts = cumulated_points;
                    lower_low_pts_trade = j + 1;
                }
                var current_dd_pts = (higher_high_pts - lower_low_pts);
                if(current_dd_pts > drawdown_pts){
                    drawdown_pts = current_dd_pts.toFixed(2);
                    dd_high_pts = higher_high_pts_trade;
                    dd_low_pts = lower_low_pts_trade;
                }

                chart_points.push([i, Math.round(cumulated_points * 100) / 100]);
                var pnl = trade.profitAndLoss.split(trade.currency)[1];
                if(pnl >= 0){
                    money_won += +pnl;
                }
                else{
                    money_lost += +pnl;
                }
                cumulated_money += +pnl;
                chart_money.push([i, cumulated_money]);

                var localDate = new Date(trade.dateUtc);
                var htmlTr = '<tr data-open="' + trade.openLevel + '" data-close="' + trade.closeLevel + '" data-points="' + points + '" class="' + (pnl > 0 ? "c-blue" : (pnl < 0 ? "c-red" : "")) + '">'
                                + '<td>' + trade.instrumentName + '</td>'
                                + '<td>' + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + '<br>' + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + '</td>'
                                + '<td>' + trade.size + '</td>'
                                + '<td style="width: 18%">O: ' + trade.openLevel + '<br/>C: ' + trade.closeLevel + '</td>'
                                + '<td>' + points + '</td>'
                                + '<td style="width: 18%">' + pnl + ' ' + accountCurrencySymbol + '</td>'
                            + '</tr>';
                $('#STATS_TRADES_LIST').append(htmlTr);
                i++;
            }
            else{
                var index = transactionsForStatistics.indexOf(trade);
                if (index > -1) {
                    transactionsForStatistics.splice(index, 1);
                }
                j--;
            }
        }



        var options = {
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true,
                    radius: 5,
                    symbol: "circle"
                },
                color: '#000',
                shadowSize: 5,
            },
            grid: {
                borderWidth: 1,
                borderColor: '#eee',
                labelMargin: 20,
                hoverable: true,
                clickable: true,
                mouseActiveRadius:6,
            },
            yaxis: {
                tickColor: '#eee',
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#9f9f9f",
                    weight: "bold"
                },
                shadowSize: 0,

            },
            xaxis: {
                tickColor: '#eee',
                show: true,
                font :{
                    lineHeight: 13,
                    style: "normal",
                    color: "#9f9f9f",
                    weight: "bold"
                },
                shadowSize: 0,
                min: 0
            },
            hooks: {
                draw  : [raw]
            }
        };


        //$('#FROM_STATS').html(fromDate);
        //$('#TO_STATS').html(toDate);
        //console.log(+$('#FUNDS').html() > 0);
        $("#points-chart, #money-chart, #capital-growth-chart, #capital-chart").unbind();
        if(+$('#FUNDS').html() > 0){
            $('#GROWTH').html(Math.round((money_lost + money_won) / ($('#FUNDS').html() - (money_lost + money_won)) * 100 * 100) / 100 + ' %');
            var startCapital = +($('#FUNDS').html() - (money_lost + money_won));
            var initialCapital = startCapital;
            var capital = [[0, startCapital]];
            var capital_growth = [[0, 0]];
            var ii = 1;
            for(var jj = 0; jj < transactionsForStatistics.length; jj++){
                var tradeForCapital = transactionsForStatistics[jj];
                if(tradeForCapital.transactionType === 'ORDRE') {
                    startCapital += +tradeForCapital.profitAndLoss.split(tradeForCapital.currency)[1];
                    capital.push([ii, startCapital]);
                    capital_growth.push([ii, Math.round((startCapital - initialCapital) / initialCapital * 100 * 100) / 100])
                }
                else{
                    var ind = transactionsForStatistics.indexOf(tradeForCapital);
                    if (ind > -1) {
                        transactionsForStatistics.splice(ind, 1);
                    }
                    jj--;
                }
                ++ii;
                //console.log(startCapital, capital);
            }
            //console.log(capital);

            $("#capital-chart").empty();
            $.plot($("#capital-chart"), [
                {data: capital, label: 'Capital'}
            ], options);
            $("#capital-growth-chart").empty();
            $.plot($("#capital-growth-chart"), [
                {data: capital_growth, label: ' Growth'}
            ], options);

            $("#capital-growth-chart").bind("plothover", function (event, pos, item) {
                if (item) {
                    //console.log(item);
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(2);

                    if(item.dataIndex > 0){
                        var t = item.dataIndex - 1,
                            tradeInfos = transactionsForStatistics[t];
                        //console.log(t);
                        //console.log(item);
                        //console.log(transactionsForStatistics[t]);
                        if(tradeInfos.transactionType == 'ORDRE'){
                            var localDate = new Date(tradeInfos.dateUtc);
                            $("#tooltip").html(item.series.label + ": <span class='" + (y > 0 ? "c-blue" : (y < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + y + "%" + "</span>"
                                + "<br>"
                                + "Trade #<span style='font-weight:lighter'>" + x + "</span>"
                                + "<br>"
                                + "<br>"
                                + "Instrument: <span style='font-weight:lighter'>" + tradeInfos.instrumentName + "</span>"
                                + "<br>"
                                + "Ref: <span style='font-weight:lighter'>" + tradeInfos.reference + "</span>"
                                + "<br>"
                                + "<br>"
                                + "Date: <span style='font-weight:lighter'>" + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + "</span>"
                                + "<br>"
                                + "Time: <span style='font-weight:lighter'>" + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + "</span>"
                                + "<br>"
                                + "<br>"
                                + "Size: <span style='font-weight:lighter'>" + tradeInfos.size + "</span>"
                                + "<br>"
                                + "Open / Close levels: <span style='font-weight:lighter'>" + tradeInfos.openLevel + " / " + tradeInfos.closeLevel + "</span>"
                                + "<br>"
                                + "Trade points / percents: <span class='" + ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) > 0 ? "c-blue" : ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) < 0 ?  "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) + " / " +(Math.round((+tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] / capital[x-1][1] * 100) * 100) / 100) + "%</span>"
                                + "<br>"
                                + "Profit and loss: <span class='" + (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] > 0 ? "c-blue" : (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] + accountCurrencySymbol + "</span>")
                                .css({top: item.pageY+20, left: item.pageX-100})
                                .fadeIn(200);
                        }
                    }

                } else {
                    $("#tooltip").hide();
                }
            });
            $("#capital-chart").bind("plothover", function (event, pos, item) {
                if (item) {
                    //console.log(item);
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(2);

                    if(item.dataIndex > 0){
                        var t = item.dataIndex - 1,
                            tradeInfos = transactionsForStatistics[t];
                        //console.log(t);
                        //console.log(item);
                        //console.log(transactionsForStatistics[t]);
                        if(tradeInfos.transactionType == 'ORDRE'){
                            var localDate = new Date(tradeInfos.dateUtc);
                            $("#tooltip").html(item.series.label + ": <span class='" + (y > initialCapital ? "c-blue" : (y < initialCapital ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + y + accountCurrencySymbol + "</span>"
                                + "<br>"
                                + "Total won / lost: <span class='" + ((y - initialCapital) > 0 ? "c-blue" : ((y - initialCapital) < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (y - initialCapital) + accountCurrencySymbol + "</span>"
                                + "<br>"
                                + "Trade #<span style='font-weight:lighter'>" + x + "</span>"
                                + "<br>"
                                + "<br>"
                                + "Instrument: <span style='font-weight:lighter'>" + tradeInfos.instrumentName + "</span>"
                                + "<br>"
                                + "Ref: <span style='font-weight:lighter'>" + tradeInfos.reference + "</span>"
                                + "<br>"
                                + "<br>"
                                + "Date: <span style='font-weight:lighter'>" + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + "</span>"
                                + "<br>"
                                + "Time: <span style='font-weight:lighter'>" + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + "</span>"
                                + "<br>"
                                + "<br>"
                                + "Size: <span style='font-weight:lighter'>" + tradeInfos.size + "</span>"
                                + "<br>"
                                + "Open / Close levels: <span style='font-weight:lighter'>" + tradeInfos.openLevel + " / " + tradeInfos.closeLevel + "</span>"
                                + "<br>"
                                + "Trade points: <span class='" + ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) > 0 ? "c-blue" : ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) < 0 ?  "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) + "</span>"
                                + "<br>"
                                + "Profit and loss: <span class='" + (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] > 0 ? "c-blue" : (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] + accountCurrencySymbol + "</span>")
                                .css({top: item.pageY+20, left: item.pageX-100})
                                .fadeIn(200);
                        }
                    }
                    else{
                        $("#tooltip").html(item.series.label + ": " + y + accountCurrencySymbol).css({top: item.pageY+20, left: item.pageX-50}).fadeIn(200);
                    }

                } else {
                    $("#tooltip").hide();
                }
            });
            /*var capital_dd = (+$('#FUNDS').html() - (money_lost + money_won));
            var higher_high_balance = capital_dd;
            var higher_high_trade = 0;
            var lower_low_balance = capital_dd;
            var lower_low_trade = 0;
            var drawdown = 0;
            var dd_high = 0;
            var dd_low = 0;
            for(var k = 0; k < transactionsForStatistics.length; k++) {
                var tradedd = transactionsForStatistics[k];
                var pnldd = tradedd.profitAndLoss.split(tradedd.currency)[1];
                capital_dd = capital_dd + +pnldd;
                if (tradedd.transactionType == 'ORDRE') {
                    if(capital_dd > higher_high_balance){
                        higher_high_balance = lower_low_balance = capital_dd;
                        higher_high_trade = lower_low_trade = k + 1;
                    }
                    if(capital_dd < lower_low_balance){
                        lower_low_balance = capital_dd;
                        lower_low_trade = k + 1;
                    }
                    var current_dd = (higher_high_balance - lower_low_balance) / higher_high_balance * 100;
                    if(current_dd > drawdown){
                        drawdown = current_dd.toFixed(2);
                        dd_high = higher_high_trade;
                        dd_low = lower_low_trade;
                    }
                }
            }*/
        }
        else{
            setTimeout(function(){
                $('#GROWTH').html(Math.round((money_lost + money_won) / ($('#FUNDS').html() - (money_lost + money_won)) * 100 * 100) / 100 + ' %');
                var startCapital = +($('#FUNDS').html() - (money_lost + money_won));
                var initialCapital = startCapital;
                var capital = [[0, startCapital]];
                var capital_growth = [[0, 0]];
                var ii = 1;
                for(var jj = 0; jj < transactionsForStatistics.length; jj++){
                    var tradeForCapital = transactionsForStatistics[jj];
                    if(tradeForCapital.transactionType === 'ORDRE') {
                        startCapital += +tradeForCapital.profitAndLoss.split(tradeForCapital.currency)[1];
                        capital.push([ii, startCapital]);
                        capital_growth.push([ii, Math.round((startCapital - initialCapital) / initialCapital * 100 * 100) / 100])
                    }
                    else{
                        var ind = transactionsForStatistics.indexOf(tradeForCapital);
                        if (ind > -1) {
                            transactionsForStatistics.splice(ind, 1);
                        }
                        jj--;
                    }
                    ++ii;
                    //console.log(startCapital, capital);
                }
                //console.log(capital);

                $("#capital-chart").empty();
                $.plot($("#capital-chart"), [
                    {data: capital, label: 'Capital'}
                ], options);
                $("#capital-growth-chart").empty();
                $.plot($("#capital-growth-chart"), [
                    {data: capital_growth, label: ' Growth'}
                ], options);

                $("#capital-growth-chart").bind("plothover", function (event, pos, item) {
                    if (item) {
                        //console.log(item);
                        var x = item.datapoint[0].toFixed(0),
                            y = item.datapoint[1].toFixed(2);

                        if(item.dataIndex > 0){
                            var t = item.dataIndex - 1,
                                tradeInfos = transactionsForStatistics[t];
                            //console.log(t);
                            //console.log(item);
                            //console.log(transactionsForStatistics[t]);
                            if(tradeInfos.transactionType == 'ORDRE'){
                                var localDate = new Date(tradeInfos.dateUtc);
                                $("#tooltip").html(item.series.label + ": <span class='" + (y > 0 ? "c-blue" : (y < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + y + "%" + "</span>"
                                    + "<br>"
                                    + "Trade #<span style='font-weight:lighter'>" + x + "</span>"
                                    + "<br>"
                                    + "<br>"
                                    + "Instrument: <span style='font-weight:lighter'>" + tradeInfos.instrumentName + "</span>"
                                    + "<br>"
                                    + "Ref: <span style='font-weight:lighter'>" + tradeInfos.reference + "</span>"
                                    + "<br>"
                                    + "<br>"
                                    + "Date: <span style='font-weight:lighter'>" + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + "</span>"
                                    + "<br>"
                                    + "Time: <span style='font-weight:lighter'>" + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + "</span>"
                                    + "<br>"
                                    + "<br>"
                                    + "Size: <span style='font-weight:lighter'>" + tradeInfos.size + "</span>"
                                    + "<br>"
                                    + "Open / Close levels: <span style='font-weight:lighter'>" + tradeInfos.openLevel + " / " + tradeInfos.closeLevel + "</span>"
                                    + "<br>"
                                    + "Trade points / percents: <span class='" + ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) > 0 ? "c-blue" : ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) < 0 ?  "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) + " / " +(Math.round((+tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] / capital[x-1][1] * 100) * 100) / 100) + "%</span>"
                                    + "<br>"
                                    + "Profit and loss: <span class='" + (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] > 0 ? "c-blue" : (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] + accountCurrencySymbol + "</span>")
                                    .css({top: item.pageY+20, left: item.pageX-100})
                                    .fadeIn(200);
                            }
                        }

                    } else {
                        $("#tooltip").hide();
                    }
                });

                $("#capital-chart").bind("plothover", function (event, pos, item) {
                    if (item) {
                        //console.log(item);
                        var x = item.datapoint[0].toFixed(0),
                            y = item.datapoint[1].toFixed(2);

                        if(item.dataIndex > 0){
                            var t = item.dataIndex - 1,
                                tradeInfos = transactionsForStatistics[t];
                            //console.log(t);
                            //console.log(item);
                            //console.log(transactionsForStatistics[t]);
                            if(tradeInfos.transactionType == 'ORDRE'){
                                var localDate = new Date(tradeInfos.dateUtc);
                                $("#tooltip").html(item.series.label + ": <span class='" + (y > initialCapital ? "c-blue" : (y < initialCapital ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + y + accountCurrencySymbol + "</span>"
                                    + "<br>"
                                    + "Total won / lost: <span class='" + ((y - initialCapital) > 0 ? "c-blue" : ((y - initialCapital) < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (y - initialCapital) + accountCurrencySymbol + "</span>"
                                    + "<br>"
                                    + "Trade #<span style='font-weight:lighter'>" + x + "</span>"
                                    + "<br>"
                                    + "<br>"
                                    + "Instrument: <span style='font-weight:lighter'>" + tradeInfos.instrumentName + "</span>"
                                    + "<br>"
                                    + "Ref: <span style='font-weight:lighter'>" + tradeInfos.reference + "</span>"
                                    + "<br>"
                                    + "<br>"
                                    + "Date: <span style='font-weight:lighter'>" + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + "</span>"
                                    + "<br>"
                                    + "Time: <span style='font-weight:lighter'>" + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + "</span>"
                                    + "<br>"
                                    + "<br>"
                                    + "Size: <span style='font-weight:lighter'>" + tradeInfos.size + "</span>"
                                    + "<br>"
                                    + "Open / Close levels: <span style='font-weight:lighter'>" + tradeInfos.openLevel + " / " + tradeInfos.closeLevel + "</span>"
                                    + "<br>"
                                    + "Trade points: <span class='" + ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) > 0 ? "c-blue" : ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) < 0 ?  "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) + "</span>"
                                    + "<br>"
                                    + "Profit and loss: <span class='" + (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] > 0 ? "c-blue" : (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] + accountCurrencySymbol + "</span>")
                                    .css({top: item.pageY+20, left: item.pageX-100})
                                    .fadeIn(200);
                            }
                        }
                        else{
                            $("#tooltip").html(item.series.label + ": " + y + accountCurrencySymbol).css({top: item.pageY+20, left: item.pageX-50}).fadeIn(200);
                        }

                    } else {
                        $("#tooltip").hide();
                    }
                });

                /*var capital_dd = (+$('#FUNDS').html() - (money_lost + money_won));
                var higher_high_balance = capital_dd;
                var higher_high_trade = 0;
                var lower_low_balance = capital_dd;
                var lower_low_trade = 0;
                var drawdown = 0;
                var dd_high = 0;
                var dd_low = 0;
                for(var k = 0; k < transactionsForStatistics.length; k++) {
                    var tradedd = transactionsForStatistics[k];
                    var pnldd = tradedd.profitAndLoss.split(tradedd.currency)[1];
                    var capital_dd = capital_dd + +pnldd;
                    if (tradedd.transactionType == 'ORDRE') {
                        if(capital_dd > higher_high_balance){
                            higher_high_balance = lower_low_balance = capital_dd;
                            higher_high_trade = lower_low_trade = k + 1;
                        }
                        if(capital_dd < lower_low_balance){
                            lower_low_balance = capital_dd;
                            lower_low_trade = k + 1;
                        }
                        var current_dd = (higher_high_balance - lower_low_balance) / higher_high_balance * 100;
                        if(current_dd > drawdown){
                            drawdown = current_dd.toFixed(2);
                            dd_high = higher_high_trade;
                            dd_low = lower_low_trade;
                        }
                    }
                }*/
            }, 2500);
        }
        $('#TRADES_WON').html(((Math.round(trade_won / (trade_won + trade_lost) * 100 * 100) / 100) || (0)));
        $('#TRADES_LOST').html(((Math.round(trade_lost / (trade_won + trade_lost) * 100 * 100) / 100) || (0)));
        $('#PTS_WON').html(Math.round(pts_won * 100) / 100);
        $('#PTS_LOST').html(Math.round(pts_lost * 100) / 100);
        $('#PTS_TOTAL').html(Math.round((pts_won + pts_lost) * 100) / 100);
        $('#AVG_PTS_WIN').html(((Math.round(pts_won / trade_won * 100) / 100) || (0)));
        $('#AVG_PTS_LOSS').html(((Math.round(pts_lost / trade_lost * 100) / 100) || (0)));
        $('#MONEY_WON').html((Math.round(money_won * 100) / 100) + ' ' + accountCurrencySymbol);
        $('#MONEY_LOST').html((Math.round(money_lost * 100) / 100) + ' ' + accountCurrencySymbol);
        $('#MONEY_TOTAL').html((Math.round((money_lost + money_won) * 100) / 100) + ' ' + accountCurrencySymbol);
        $('#PTS_PER_TRADE').html(((Math.round((pts_won + pts_lost)/(trade_won + trade_lost) * 100) / 100) || (0)));
        $('#AVG_WIN').html((Math.round(money_won / trade_won * 100) / 100) + ' ' + accountCurrencySymbol);
        $('#AVG_LOSS').html((Math.round(money_lost / trade_lost * 100) / 100) + ' ' + accountCurrencySymbol);
        $('#PROFIT_FACTOR').html(((Math.round((money_won / Math.abs(money_lost)) * 100) / 100) || (0)));
        $('#DRAWDOWN').html(drawdown_pts);
        $('#NB_TRADES').html((trade_won + trade_lost));
    }
    else{
        //$('#FROM_STATS').html(fromDate);
        //$('#TO_STATS').html(toDate);
        $('#GROWTH, #TRADES_WON, #TRADES_LOST, #PTS_WON, #PTS_LOST, #PTS_TOTAL, #PTS_PER_TRADE, #AVG_PTS_WIN, #AVG_PTS_LOSS, #MONEY_WON, #MONEY_LOST, #MONEY_TOTAL, #MONEY_PER_TRADE, #AVG_WIN, #AVG_LOSS, #NB_TRADES').html('0');
        $('#PROFIT_FACTOR').html('-');
    }
        
    /* Chart Options */
    function raw(plot, ctx) {
        var data = plot.getData();
        var axes = plot.getAxes();
        var offset = plot.getPlotOffset();
        //console.log(data);
        for (var i = 0; i < data.length; i++) {
            var series = data[i];
            for (var j = 0; j < series.data.length; j++) {
                //console.log('j: ' + series.data[j]);
                //console.log('j-1: ' + series.data[j-1]);
                if(j > 0){
                    //console.log(j);
                    var d = (series.data[j]);
                    var x = offset.left + axes.xaxis.p2c(d[0]);
                    var y = offset.top + axes.yaxis.p2c(d[1]);
                    var r = 5;
                    //console.log(dd_high_pts);
                    //console.log(dd_low_pts);
                    if(j == dd_high_pts || j == dd_low_pts){
                        r = 10;
                    }
                    ctx.lineWidth = 2;
                    ctx.beginPath();
                    ctx.arc(x,y,r,0,Math.PI*2,true);
                    ctx.closePath();
                    if(series.data[j][1] > series.data[j - 1][1]){
                        ctx.fillStyle = "#2196f3";
                    }
                    else if(series.data[j][1] == series.data[j - 1][1]){
                        ctx.fillStyle = "#ff9800";
                    }
                    else{
                        ctx.fillStyle = "#f44336";
                    }
                    ctx.fill();
                }
            }
        }
    }
    
    /* Regular Line Chart */
    $("#points-chart").empty();
    $.plot($("#points-chart"), [
        {data: chart_points, label: 'Points'}
    ], options);
    $("#money-chart").empty();
    $.plot($("#money-chart"), [
        {data: chart_money, label: 'Money'}
    ], options);
    
    $("#tooltip").css({
			position: "absolute",
			display: "none",
			border: "2px solid #9f9f9f",
			padding: "2px",
			"background-color": "#fff",
			opacity: 1,
            "font-weight": "bold"
		}).appendTo("body");
    
    $("#points-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(1);

                if(item.dataIndex > 0){
                    var t = item.dataIndex - 1,
                    tradeInfos = transactionsForStatistics[t];
                    //console.log(t);
                    //console.log(item);
                    //console.log(transactionsForStatistics[t]);
                    if(tradeInfos.transactionType == 'ORDRE'){
                        var localDate = new Date(tradeInfos.dateUtc);
                        $("#tooltip").html(item.series.label + " - Total: <span class='" + (y > 0 ? "c-blue" : (y < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + y + "</span>"
                            + "<br>"
                            + "Trade #<span style='font-weight:lighter'>" + x + "</span>"
                            + "<br>"
                            + "<br>"
                            + "Instrument: <span style='font-weight:lighter'>" + tradeInfos.instrumentName + "</span>"
                            + "<br>"
                            + "Ref: <span style='font-weight:lighter'>" + tradeInfos.reference + "</span>"
                            + "<br>"
                            + "<br>"
                            + "Date: <span style='font-weight:lighter'>" + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + "</span>"
                            + "<br>"
                            + "Time: <span style='font-weight:lighter'>" + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + "</span>"
                            + "<br>"
                            + "<br>"
                            + "Size: <span style='font-weight:lighter'>" + tradeInfos.size + "</span>"
                            + "<br>"
                            + "Open / Close levels: <span style='font-weight:lighter'>" + tradeInfos.openLevel + " / " + tradeInfos.closeLevel + "</span>"
                            + "<br>"
                            + "Trade points: <span class='" + ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) > 0 ? "c-blue" : ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) < 0 ?  "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) + "</span>"
                            + "<br>"
                            + "Profit and loss: <span class='" + (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] > 0 ? "c-blue" : (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] + accountCurrencySymbol + "</span>")
                            .css({top: item.pageY+20, left: item.pageX-100})
                            .fadeIn(200);
                    }
                }

            } else {
                $("#tooltip").hide();
            }
		});

    $("#money-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(1);


                if(item.dataIndex > 0){
                    var t = item.dataIndex - 1,
                    tradeInfos = transactionsForStatistics[t];
                    //console.log(t);
                    //console.log(item);
                    //console.log(transactionsForStatistics[t]);
                    if(tradeInfos.transactionType == 'ORDRE'){
                        var localDate = new Date(tradeInfos.dateUtc);
                        $("#tooltip").html(item.series.label + " - Total: <span class='" + (y > 0 ? "c-blue" : (y < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + y + accountCurrencySymbol + "</span>"
                            + "<br>"
                            + "Trade #<span style='font-weight:lighter'>" + x + "</span>"
                            + "<br>"
                            + "<br>"
                            + "Instrument: <span style='font-weight:lighter'>" + tradeInfos.instrumentName + "</span>"
                            + "<br>"
                            + "Ref: <span style='font-weight:lighter'>" + tradeInfos.reference + "</span>"
                            + "<br>"
                            + "<br>"
                            + "Date: <span style='font-weight:lighter'>" + ('0' + localDate.getDate()).slice(-2) + '-' + ('0' + (localDate.getMonth() + 1)).slice(-2) + '-' + localDate.getFullYear() + "</span>"
                            + "<br>"
                            + "Time: <span style='font-weight:lighter'>" + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + "</span>"
                            + "<br>"
                            + "<br>"
                            + "Size: <span style='font-weight:lighter'>" + tradeInfos.size + "</span>"
                            + "<br>"
                            + "Open / Close levels: <span style='font-weight:lighter'>" + tradeInfos.openLevel + " / " + tradeInfos.closeLevel + "</span>"
                            + "<br>"
                            + "Trade points: <span class='" + ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) > 0 ? "c-blue" : ((tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) < 0 ?  "c-deeporange" : "")) + "' style='font-weight:lighter'>" + (tradeInfos.size.indexOf("+") > -1 ? (Math.round((tradeInfos.closeLevel - tradeInfos.openLevel) * 100) / 100) : (Math.round((tradeInfos.openLevel - tradeInfos.closeLevel) * 100) / 100)) + "</span>"
                            + "<br>"
                            + "Profit and loss: <span class='" + (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] > 0 ? "c-blue" : (tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] < 0 ? "c-deeporange" : "")) + "' style='font-weight:lighter'>" + tradeInfos.profitAndLoss.split(tradeInfos.currency)[1] + accountCurrencySymbol + "</span>")
                            .css({top: item.pageY+20, left: item.pageX-100})
                            .fadeIn(200);
                    }
                }
            } else {
                $("#tooltip").hide();
            }
		});
    
}
    
/*
 * Retrieve transactions
 */
function retrieveTransactions(fromDate, toDate){
    if(fromDate == undefined && toDate == undefined){
        fromDate = toDate = getTodayDate();
    }
    // Create a GET request to /history/transactions/...
   var req = new Request();
   req.method = "GET";
    req.url = urlRoot + "/history/transactions?type=ALL&from=" + fromDate.split('-')[2] + "-" + fromDate.split('-')[1] + "-" + fromDate.split('-')[0] + "T00%3A00%3A00&to=" + toDate.split('-')[2] + "-" + toDate.split('-')[1] + "-" + toDate.split('-')[0] + "T23%3A59%3A59&pageSize=0";

   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "Version": "2"
   };

   // Send the request via a Javascript AJAX call
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
            // Something went wrong
            handleHTTPError(response);
         },
         success: function (response, status, data) {
             //console.log(response);
             retrievedTransactions = response.transactions;
         }
      });
   } catch (e) {
      handleException(e);
   }
}
    
/*
 * Populate the history table if shown
 */
function populateHistoryTable(){
    var dailyTrades = 0;
    if(retrievedTransactions){
        $('#TRADES_LIST').empty();
        for (var transaction in retrievedTransactions){
            var trade = retrievedTransactions[transaction];
            //console.log(trade);
            if(trade.transactionType == 'ORDRE'){
                dailyTrades++;
                var localDate = new Date(trade.dateUtc);
                var points = trade.size.indexOf("+") > -1 ? (Math.round((trade.closeLevel - trade.openLevel) * 100) / 100) : (Math.round((trade.openLevel - trade.closeLevel) * 100) / 100);
                var pnl = trade.profitAndLoss.split(trade.currency)[1];
                var htmlTr = '<tr data-open="' + trade.openLevel + '" data-close="' + trade.closeLevel + '" data-points="' + points + '" class="' + (pnl > 0 ? "c-blue" : (pnl < 0 ? "c-red" : "")) + '">'
                                + '<td>' + trade.instrumentName + '</td>'
                                + '<td>' + ('0' + localDate.getHours()).slice(-2) + ":" + ('0' + localDate.getMinutes()).slice(-2) + '</td>'
                                + '<td>' + trade.size + '</td>'
                                + '<td style="width: 18%">O: ' + trade.openLevel + '<br/>C: ' + trade.closeLevel + '</td>'
                                + '<td>' + points + '</td>'
                                + '<td style="width: 18%">' + pnl + ' ' + accountCurrencySymbol + '</td>'
                            + '</tr>';
                $('#TRADES_LIST').append(htmlTr);
            }
        }
    }
    $('#PNLT').html(dailyTrades);
}

/*
 * Get today date
 */
function getTodayDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'-'+mm+'-'+yyyy;
    return today;
}
/*
 * Function to display an account update (WOU, OPU) message and update the positions list
 */
function showAccountStatusUpdate(message) {
   if (message) {

      //console.log('Account update received: ' + message);
   }

}

/*
 * Request object
 */
function Request(o) {
   this.headers = {"Content-Type": "application/json; charset=UTF-8", "Accept": "application/json; charset=UTF-8"};
   this.body = "";
   this.method = "";
   this.url = "";
}

/*
 * Exception handler - displays details of the exception on the screen
 */
function handleException(exception) {
    swal({   
        title: "An error occured",   
        text: exception,   
        showConfirmButton: true,
        type: "warning", 
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "OK!"
    });
}

/*
 * Handle an HTTP error
 */
function handleHTTPError(response) {
   swal({   
        title: "An HTTP error occured",   
        text: response.responseText,   
        showConfirmButton: true,
        type: "warning", 
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "OK!"
    });
}

/*
 * Handle an trade error
 */
function handleTradeError(title, response) {
   swal({   
        title: title,   
        text: response,   
        showConfirmButton: true,
        type: "warning", 
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "OK!"
    });
}
    
$('.editable').editable({
    format: 'dd-mm-yyyy',    
    viewformat: 'dd-mm-yyyy',    
    datepicker: {
            weekStart: 1
       },
    showbuttons: false
});
    
$('#UPDATE_STATS').on(clickEventTrigger, function(e){
    e.preventDefault();
    updateStatistics($('#FROM_STATS').html(), $('#TO_STATS').html());
})
    
/*
 * Function to blur funds for image capture
 */
$('#FUNDS, #AVAILABLE_TO_DEAL').on(clickEventTrigger, function(e){
    e.preventDefault();
    $('#FUNDS, #AVAILABLE_TO_DEAL').toggleClass('blurry-text');
});
$('#HIDE_STATS_VALUES').on(clickEventTrigger, function(e){
    e.preventDefault();
    $('#GROWTH, #MONEY_TOTAL').toggleClass('blurry-text');
});

/*
 * Show options type for each alarm
 */
$('input[name=alarmType]').on('click', function(){
    switch($(this).val()){
        case '0_50_LEVEL':
            $('#0_50_LEVEL_MARGIN_INPUT').removeClass('hidden');
            $('#LEVEL_INPUT').addClass('hidden');
            $('#LEVEL_INPUT_MARGIN').addClass('hidden');
            $('#PP_LEVEL_METHOD_SELECT').addClass('hidden');
            $('#PP_LEVEL_PERIOD_SELECT').addClass('hidden');
            $('#PP_LEVEL_MARGIN_INPUT').addClass('hidden');
            $('#0_50_LEVEL_MARGIN').focus();
            break;
            
        case 'PP_LEVEL':
            $('#PP_LEVEL_METHOD_SELECT').removeClass('hidden');
            $('#PP_LEVEL_PERIOD_SELECT').removeClass('hidden');
            $('#PP_LEVEL_MARGIN_INPUT').removeClass('hidden');
            $('#0_50_LEVEL_MARGIN_INPUT').addClass('hidden');
            $('#LEVEL_INPUT').addClass('hidden');
            $('#LEVEL_INPUT_MARGIN').addClass('hidden');
            break;
            
        case 'LEVEL':
            $('#0_50_LEVEL_MARGIN_INPUT').addClass('hidden');
            $('#LEVEL_INPUT').removeClass('hidden');
            $('#LEVEL_INPUT_MARGIN').removeClass('hidden');
            $('#PP_LEVEL_METHOD_SELECT').addClass('hidden');
            $('#PP_LEVEL_PERIOD_SELECT').addClass('hidden');
            $('#PP_LEVEL_MARGIN_INPUT').addClass('hidden');
            $('#LEVEL').focus();
            break;
    }
});
    
$('#settings').on(clickEventTrigger, function(e){
    e.preventDefault();
    $('#modalSettings').modal();
});

    
/*
 * User interface login button callback function
 */
function login(username, passwd, api, demo) {

   // Get username and password from user interface fields
   apiKey = (api != undefined ? CryptoJS.AES.decrypt(api, cryptSalt).toString(CryptoJS.enc.Utf8) : $("#apikey").val());
   identifier = username || $("#username").val();
   var password = (passwd != undefined ? CryptoJS.AES.decrypt(passwd, cryptSalt).toString(CryptoJS.enc.Utf8) : $("#password").val());
   if(demo == 'true' || $('#demo')[0].checked){
       urlRoot = 'https://demo-api.ig.com/gateway/deal';
        $('#header').css('background-color', '#2196F3');
   }
    else{
       urlRoot = 'https://api.ig.com/gateway/deal';
       isReal = true;
        $('#header').css('background-color', '#607D8B');
    }
    

   if (apiKey=="" || identifier=="" || password=="") {
       return false;
   }

   password = encryptedPassword(password);
   //console.log("Encrypted password " + password);

   // Create a login request, ie a POST request to /session
   var req = new Request();
   req.method = "POST";
   req.url = urlRoot + "/session";

   // Set up standard request headers, i.e. the api key, the request content type (JSON), 
   // and the expected response content type (JSON)
   req.headers = {
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "X-IG-API-KEY": apiKey,
      "Version": "2"
   };

   // Set up the request body with the user identifier (username) and password
   var bodyParams = {};
   bodyParams["identifier"] = identifier;
   bodyParams["password"] = password;
   bodyParams["encryptedPassword"] = true;
   req.body = JSON.stringify(bodyParams);

   // Prettify the request for display purposes only
   //$("#request_data").text(js_beautify(req.body) || "");

   // Send the request via a Javascript AJAX call
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         data: req.body,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         success: function (response, status, data) {

            // Successful login 
            // Extract account and client session tokens, active account id, and the Lightstreamer endpoint,
            // as these will be required for subsequent requests
            account_token = data.getResponseHeader("X-SECURITY-TOKEN");
            //console.log("X-SECURITY-TOKEN: " + account_token);
            client_token = data.getResponseHeader("CST");
            //console.log("CST: " + client_token);
            accountId = response.currentAccountId;
            lsEndpoint = response.lightstreamerEndpoint;
            accountCurrency = response.currencyIsoCode;
            switch(accountCurrency){
                case 'EUR':
                    accountCurrencySymbol = '€';
                    break;
                    
                case 'USD':
                    accountCurrencySymbol = '$';
                    break;
                    
                case 'GBP':
                    accountCurrencySymbol = '£';
                    break;
                    
                default:
                    accountCurrencySymbol = accountCurrency;
                    break;
            }
            $('.accountCurrencySymbol').each(function(){
                $(this).html(accountCurrencySymbol);
            });
            //console.log(response);
            //console.log(status);
            //console.log(data);
            // Prettify response for display purposes only
            //$("#response_data").text(js_beautify(data.responseText) || "");

            // Show logged in status message on screen
             
            $('#top-search, #showCurrency, #showHistory, #showReport, .fixed-action-button, #alarms, #logout, #logout_divider, #tradingcard, #settings').removeClass('hidden');
            $('#connection-a').removeClass('c-red').addClass('c-lightgreen');
            $('#connection-icon').removeClass('zmdi-cast').addClass('zmdi-cast-connected');
            $('#connection-li-a').removeClass('bgm-red').addClass('bgm-lightgreen');
            $('#connection-li-icon').removeClass('zmdi-close').addClass('zmdi-shield-check');
            $('#connection-text').html('Logged in as ' + accountId);
            $('#connectioncard').slideUp();
            //notify("Logged in as " + accountId, "success");
            $('.fixed-action-button').on(clickEventTrigger, function(){
                closeAllTrades();
            });
             
             
              connectToLightstreamer();
              subscribeToLightstreamerTradeUpdates();
              subscribeToAccountUpdates();
              showTradingPane();
              retrievePositions();
              updateDailyPnl();
              updateStatistics($('#FROM_STATS').html(), $('#TO_STATS').html());
             //console.log(localStorage);
             //console.log(JSON.parse(localStorage.getItem('Account-' + identifier)));
             if(localStorage.getItem('Account-' + identifier) !== null){
                 $('#settingsUnavailable').toggleClass('hidden');
             }
             loadSettings(true);
             //timeout = setTimeout(function(){checkTradingAllowed()}, 500);
         },
         error: function (response, status, error) {
             
            // Login failed, usually because the login id and password aren't correct
            handleHTTPError(response);
         }
      });
   } catch (e) {
        handleException(e);
   }

    return true;

}

function checkTradingAllowed(){
    clearTimeout(timeout);
    if(isReal){
        var ct = new Date();
        var checkInterval = null;
        var t1start = new Date(ct.getFullYear(), ct.getMonth(), ct.getDate(), 9, 0, 0, 0);
        var t1stop = new Date(ct.getFullYear(), ct.getMonth(), ct.getDate(), 12, 00, 0, 0);
        var t1tomorrowstart = new Date();
        t1tomorrowstart.setDate(t1start.getDate() + 1);
        if(ct.getTime() < t1start.getTime()){
            if(Object.keys(openTrades).length == 0 && !statsShown){
                tradingAllowed = false;
                swal({
                    title: "Trading application locked!!!",
                    text: "Due to trading restriction, the app is locked",
                    type: "warning",
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: "Show me my stats!",
                    cancelButtonText: "Logout!",
                    closeOnCancel: false
                }, function(isConfirm){
                    if(isConfirm){
                        statsShown = !statsShown;
                        $('#REPORT, #tradingcard').toggleClass('hidden');
                    }
                    else{
                        logout();
                    }
                });
                checkInterval = t1start.getTime() - ct.getTime();
            }
            else if (Object.keys(openTrades).length > 0){
                checkInterval = 1000;
            }
            else{
                clearTimeout(timeout);
            }
        }
        if(ct.getTime() >= t1start.getTime()){
            if(ct.getTime() <= t1stop.getTime()){
                if(!tradingAllowed){
                    swal({
                        title: "Trading application allowed!!!",
                        text: "Trade safe!",
                        type: "success",
                        timer: 1000,
                        showConfirmButton: true
                    });
                }
                tradingAllowed = true;
                checkInterval = t1stop.getTime() - ct.getTime();
            }
            else{
                if(Object.keys(openTrades).length == 0 && !statsShown){
                    tradingAllowed = false;
                    swal({
                        title: "Trading application locked!!!",
                        text: "Due to trading restriction, the app is locked",
                        type: "warning",
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: "Show me my stats!",
                        cancelButtonText: "Logout!",
                        closeOnCancel: false
                    }, function(isConfirm){
                        if(isConfirm){
                            statsShown = !statsShown;
                            $('#REPORT, #tradingcard').toggleClass('hidden');
                        }
                        else{
                            logout();
                        }
                    });
                    checkInterval = t1tomorrowstart.getTime() - ct.getTime();
                }
                else if (Object.keys(openTrades).length > 0){
                    checkInterval = 1000;
                }
                else{
                    clearTimeout(timeout);
                }
            }
        }
        if(checkInterval !== null){
            //timeout = setTimeout(function(){checkTradingAllowed()}, checkInterval);
        }
    }
}
    
/*
 * Load saved account settings
 */
function loadSettings(fullReload){
    settings = JSON.parse(localStorage.getItem('Account-' + identifier)) || account;
    clickEventTrigger = settings.click_event_trigger;
    //console.log(settings);
    // Set header color
    $('#header').css('background-color', settings.header_color);
    $('input[value=' + settings.header_color + ']').prop('checked', true);
    $('input[value=' + settings.click_event_trigger + ']').prop('checked', true);
    $('#autocalculate').prop('checked', settings.lots_size.autocalculate);
    $('#autofill').prop('checked', settings.lots_size.autofill);
    $('#SLLotsCalculator').val(settings.lots_size.stop);
    $('#PercentLotsCalculator').val(settings.lots_size.percent);
    $('#autolock').prop('checked', settings.locker.autolock);
    $('#autoLockTime').val(settings.locker.autolocktime);
    $('#autologout').prop('checked', settings.logout.autologout);
    $('#autoLogOutTime').val(settings.logout.autologouttime);
    if(Object.keys(settings.favorite_epics).length){
        $('#favoriteEpicsList').empty();
        for(var epic in settings.favorite_epics){
            if(!$('#HEADER_' + epic.split(":")[1].replace(/\./g, "_")).length && fullReload){
                subscription(epic);
            }
            $('#favoriteEpicsList').append('<div class="favoriteEpicList p-20"><button id="LOAD_FAVORITE_EPIC_' + epic.split(":")[1].replace(/\./g, "_") +'" data-epic="' + epic + '" class="btn btn-xs waves-effect pull-right loadFavoriteEpic"><i class="zmdi zmdi-mail-send"> Load</i></button><button id="DELETE_FAVORITE_EPIC_' + epic.split(":")[1].replace(/\./g, "_") +'" data-epic="' + epic + '" data-name="' + settings.favorite_epics[epic] + '" class="btn btn-danger btn-xs waves-effect pull-right m-r-5 deleteFavoriteEpic"><i class="zmdi zmdi-delete"> Delete</i></button><strong>' + settings.favorite_epics[epic] + '</strong><br/><small>' + epic + '</small></div>');

            $('#DELETE_FAVORITE_EPIC_' + epic.split(":")[1].replace(/\./g, "_")).click(function(e){
                e.preventDefault();
                toggleFavoriteEpic($(this).attr('data-epic'), $(this).attr('data-name'));
            });
            $('#LOAD_FAVORITE_EPIC_' + epic.split(":")[1].replace(/\./g, "_")).click(function(e){
                e.preventDefault();
                if(!$('#HEADER_' + $(this).attr('data-epic').split(":")[1].replace(/\./g, "_")).length){
                    subscription($(this).attr('data-epic'));
                }
            });
        }
    }
    else{
        $('#favoriteEpicsList').html('<div class="jumbotron text-center"><h4>No favorite epics yet!!!</h4></div>');
    }
    
    if(fullReload){
        activityTimeout = new Timeout(inActive, settings.locker.autolocktime * 60 * 1000);
        logoutTimeout = new Timeout(inActiveLogOut, settings.logout.autologouttime * 60 * 1000);
    }
    
    if(settings.locker.autolock){
        if($('html').hasClass('ismobile')){
            $(document).bind('touchmove', function(){
                resetActive();
            });
        }
        else{
            $(document).bind('mousemove', function(){
                resetActive();
            });
        }
    }else{
        activityTimeout.clear();
    }
    
    if(settings.logout.autologout){
        if($('html').hasClass('ismobile')){
            $(document).bind('touchmove', function(){
                resetActiveLogOut();
            });
        }
        else{
            $(document).bind('mousemove', function(){
                resetActiveLogOut();
            });
        }
    }else{
        logoutTimeout.clear();
    }
}
    
function Timeout(fn, interval) {
    var id = setTimeout(fn, interval);
    this.cleared = false;
    this.clear = function () {
        this.cleared = true;
        clearTimeout(id);
    };
}
    
function resetActiveLogOut(){
    logoutTimeout.clear();
    logoutTimeout = new Timeout(inActiveLogOut, settings.logout.autologouttime * 60 * 1000);
}

function inActiveLogOut(){
    if(settings.logout.autologout){
        logout(true);
    }
}
    
function resetActive(){
    if(!settings.locker.islocked){
        activityTimeout.clear();
        activityTimeout = new Timeout(inActive, settings.locker.autolocktime * 60 * 1000);
    }
}

function inActive(){
    if(settings.locker.autolock){
        settings.locker.islocked = true;
        swal({   
            title: "App locked",   
            text: "This is just for test only, wait for password input",   
            type: "warning",   
            showCancelButton: false,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, unlock it!",   
            closeOnConfirm: true 
        }, function(){  
            settings.locker.islocked = false; 
            resetActive();
        });
    }
    else{
        activityTimeout.clear();
    }
}

    function copyToClipboard(content) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = content;

        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        return succeed;
    }
    
/*
 * Settings saver
 */
function saveSettings(fullReload){
    if(localStorage.getItem('Account-' + identifier) !== null){
        localStorage.setItem('Account-' + identifier, JSON.stringify(settings));
    }
    loadSettings(fullReload);
    //console.log(settings);
}
    
/*
* User interface control functions
*/

$(document).delegate('#TRADES_LIST tr, #STATS_TRADES_LIST tr', clickEventTrigger, function(e){
    e.preventDefault();
    var toCopy = $(this).attr('data-open') + ' -> ' + $(this).attr('data-close') + ' => ' + (+$(this).attr('data-points') > 0 ? '+' : '') + $(this).attr('data-points');
    var res = copyToClipboard(toCopy);
    if(res){
        notify('Copied', 'info');
    }
    else{
        notify('Can\'t copy!!!', 'danger');
    }
});

$('#saveAccountInSession').on(clickEventTrigger, function(){
    localStorage.setItem('Account-' + identifier, JSON.stringify(settings));
    $('#settingsUnavailable').toggleClass('hidden');
    notify('Account saved', 'success');
});

$('input[name=click_event_trigger]').change(function(){
    clickEventTrigger = $(this).val();
    settings.click_event_trigger = $(this).val();
    saveSettings(false);
    //console.log(clickEventTrigger);
});

$('input[name=header_color]').change(function(){
    $('#header').css('background-color', $(this).val());
    settings.header_color = $(this).val();
    saveSettings(false);
});
    
$('#autocalculate').change(function(){
    settings.lots_size.autocalculate = ($(this).prop('checked') ? true : false);
    saveSettings(false);
});   
    
$('#autofill').change(function(){
    settings.lots_size.autofill = ($(this).prop('checked') ? true : false);
    saveSettings(false);
}); 
    
$('#SLLotsCalculator').change(function(){
    settings.lots_size.stop = +$(this).val();
    saveSettings(false);
});
    
$('#PercentLotsCalculator').change(function(){
    settings.lots_size.percent = +$(this).val();
    saveSettings(false);
});
    
$('#autolock').change(function(){
    settings.locker.autolock = ($(this).prop('checked') ? true : false);
    saveSettings(false);
}); 
    
$('#autoLockTime').change(function(){
    settings.locker.autolocktime = +$(this).val();
    saveSettings(false);
});
    
$('#autologout').change(function(){
    settings.logout.autologout = ($(this).prop('checked') ? true : false);
    saveSettings(false);
}); 
    
$('#autoLogOutTime').change(function(){
    settings.logout.autologouttime = +$(this).val();
    saveSettings(false);
}); 

$('#loginButton').on(clickEventTrigger, function () {
    if($('#conditions').prop('checked')){
        $('#loginButton').prop('disabled', true);
        var savedAccount = localStorage.getItem('Account-' + $('#username').val());
        var username = CryptoJS.AES.encrypt($('#username').val(), cryptSalt);
        var password = CryptoJS.AES.encrypt($('#password').val(), cryptSalt);
        var apiKey = CryptoJS.AES.encrypt($('#apikey').val(), cryptSalt);
        var demo = $('#demo').prop('checked');
        account = null;
        if($('#savelogin').prop('checked')){
            if(savedAccount !== null){
                savedAccount = JSON.parse(savedAccount);
                savedAccount.credentials.password = password.toString();
                savedAccount.credentials.apiKey = apiKey.toString();
                account = savedAccount;
            }
            else{
                account = {
                    'credentials': {
                        'username': username.toString(),
                        'password': password.toString(),
                        'apiKey': apiKey.toString(),
                        'demo': demo.toString()
                    },
                    'click_event_trigger': 'mouseup',
                    'header_color': (demo.toString() == 'true' ? '#2196F3' : '#607D8B'),
                    'favorite_epics': {},
                    'lots_size': {
                        autocalculate: false,
                        autofill: false,
                        percent: 0,
                        stop: 0
                    },
                    locker: {
                        autolock: false,
                        autolocktime: 30,
                        islocked: false
                    },
                    logout: {
                        autologout: true,
                        autologouttime: 60
                    },
                    saved: true
                };
            }
            localStorage.setItem('Account-' + $('#username').val(), JSON.stringify(account));
        }
        else{
            if(savedAccount !== null){
                removeAccount($('#username').val());
            }
            account = {
                    'credentials': {
                        'username': username.toString(),
                        'password': password.toString(),
                        'apiKey': apiKey.toString(),
                        'demo': demo.toString()
                    },
                    'click_event_trigger': 'mouseup',
                    'header_color': (demo.toString() == 'true' ? '#2196F3' : '#607D8B'),
                    'favorite_epics': {},
                    'lots_size': {
                        autocalculate: false,
                        autofill: false,
                        percent: 0,
                        stop: 0
                    },
                    locker: {
                        autolock: false,
                        autolocktime: 30,
                        islocked: false
                    },
                    logout: {
                        autologout: true,
                        autologouttime: 60
                    },
                    saved: false
                };
        }
        //console.log(localStorage);
       if (!login()) {
           notify('Username, Password and API Key are required!', 'danger');
       }
    }else{
        notify('You have to read, understand and accept the terms and conditions', 'danger');
    }
    $('#loginButton').prop('disabled', false);
});
    
/*
 * Remove a single account from local storage
 */
function removeAccount(account){
    localStorage.removeItem('Account-' + account);
    $('#' + account).remove();
    //console.log(localStorage);
}
    
function logout(notification){
    // Create a DELETE request to /session
   var req = new Request();
   req.method = "DELETE";
   req.url = urlRoot + "/session";
    
   // Set up the request headers, i.e. the api key, the account security session token, the client security token, 
   // the request content type (JSON), and the expected response content type (JSON)      
   req.headers = {
      "X-IG-API-KEY": apiKey,
      "X-SECURITY-TOKEN": account_token,
      "CST": client_token,
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
      "Version": "1"
   };

   // Send the request via a Javascript AJAX call
   var resultData;
   try {
      $.ajax({
         type: req.method,
         url: req.url,
         headers: req.headers,
         async: false,
         mimeType: req.binary ? 'text/plain; charset=x-user-defined' : null,
         error: function (response, status, error) {
         },
         success: function (response, status, data) {
         },
          complete: function(){
              if(notification){
                  swal({
                          title: "You're not logged!",
                          text: "Inactivity auto logout.",
                          type: "warning",
                          showCancelButton: false,
                          confirmButtonText: "OK take me to home!!!"
                      },
                      function(){
                          window.location = '/';
                      });
              }
              else{
                  window.location = '/';
              }
          }
      });
   } catch (e) {
      handleException(e);
   }
}
    
$(document).ready(function(){
    var accountsNumber = 0;
    for(var i=0; i<localStorage.length; i++) {
        
        var key = localStorage.key(i);
        var value = localStorage[key];
        if(key.split('-')[0] == 'Account'){
            if(accountsNumber == 0){
                $('#savedAccountList').empty();
            }
            accountsNumber++;
            var account = JSON.parse(value);
            //console.log(account);
            var passwordHidden = '';
            for(var j = 0; j < CryptoJS.AES.decrypt(account.credentials.password, cryptSalt).toString(CryptoJS.enc.Utf8).length; j++){
                passwordHidden += '*';
            }
            var username = CryptoJS.AES.decrypt(account.credentials.username, cryptSalt).toString(CryptoJS.enc.Utf8);
            var password = CryptoJS.AES.decrypt(account.credentials.password, cryptSalt).toString(CryptoJS.enc.Utf8);
            var apiKey = CryptoJS.AES.decrypt(account.credentials.apiKey, cryptSalt).toString(CryptoJS.enc.Utf8);
            $('#savedAccountList').append('<a href="#" id="' + username + '" class="savedAccount" data-username="' + username + '" data-password="' + account.credentials.password + '" data-apiKey="' + account.credentials.apiKey + '" data-demo="' + (account.credentials.demo == 'true' ? 'true' : 'false') + '">'
                                                + '<strong>'
                                                    + '#' + accountsNumber + " - " + username + ' - ' + (account.credentials.demo == 'true' ? 'DEMO' : 'LIVE')
                                                + '</strong><br/>' 
                                                + passwordHidden + '<br/>' 
                                                + apiKey.substring(0, 20) + '...'
                                            + '</a>');
            $.contextMenu({
                selector: '#' + username, 
                callback: function(key, options) {
                    switch(key){
                        case 'connect':
                            login($(options.selector).attr('data-username'), $(options.selector).attr('data-password'), $(options.selector).attr('data-apiKey'), $(options.selector).attr('data-demo'));
                            break;
                            
                        case 'edit':
                            $('#username').val($(options.selector).attr('data-username'));
                            $('#password').val(CryptoJS.AES.decrypt($(options.selector).attr('data-password'), cryptSalt).toString(CryptoJS.enc.Utf8));
                            $('#apikey').val(CryptoJS.AES.decrypt($(options.selector).attr('data-apiKey'), cryptSalt).toString(CryptoJS.enc.Utf8));
                            $(options.selector).attr('data-demo') == 'true' ? $('#demo').prop('checked', true) : $('#demo').prop('checked', false);
                            $('#savelogin').prop('checked', true);
                            break;

                        case 'delete':
                            swal({   
                                title: "Are you sure?",   
                                text: "This will remove your account and its settings from your local storage definitely!",   
                                type: "warning",   
                                showCancelButton: true,   
                                confirmButtonColor: "#DD6B55",   
                                confirmButtonText: "Yes, delete it!",   
                                closeOnConfirm: false 
                            }, function(){  
                                removeAccount($(options.selector).attr('data-username'));
                                swal("Deleted!", "Your account has been deleted.", "success"); 
                            });
                            break;
                    }
                },
                items: {
                    "connect": {name: '<i id="connection-icon" class="zmdi zmdi-cast-connected"></i> Connection'},
                    "edit": {name: '<i id="connection-icon" class="zmdi zmdi-edit"></i> Edit'},
                    "sep1": "---------",
                    "delete": {name: '<i id="connection-icon" class="zmdi zmdi-delete"></i> Remove from local storage'}
                }
            });
        }
    }
    
    $('.savedAccount').on(clickEventTrigger, function(e){
        //console.log(e.preventDefault());
        //console.log(e.isDefaultPrevented());
        if(e.which == 1){
            login($(this).attr('data-username'), $(this).attr('data-password'), $(this).attr('data-apiKey'), $(this).attr('data-demo'));
        }
    });
});
    
$('#logoutLs').on(clickEventTrigger, function(e){
    e.preventDefault();
    logout();
});

$('#searchEpic').keyup(function () {
    if($('#searchEpic').val().length >= 3){
        search();
    }
});
    
$('#saveAlarm').on(clickEventTrigger, function(e){
    e.preventDefault();
    saveAlarm();
});
    
$('#showReportIcon').on(clickEventTrigger, function(e){
    e.preventDefault();
    statsShown = !statsShown;
    $('#REPORT, #tradingcard').toggleClass('hidden');
    //checkTradingAllowed();
});
    
$('#showHistoryIcon').on(clickEventTrigger, function(e){
    e.preventDefault();
    var history = '<div class="col-sm-6" id="HYSTORY">'
                        + '<div class="card">'
                            + '<div class="card-header bgm-bluegray">'
                                + '<h2>History - <span id="PNLT">0</span> trade(s)</h2>'
                                + '<ul class="actions">'
                                    + '<li class="dropdown">'
                                        + '<a href="#" data-toggle="dropdown">'
                                            + '<i class="zmdi zmdi-more-vert"></i>'
                                        + '</a>'
                                        + '<ul class="dropdown-menu dropdown-menu-right">'
                                            + '<li>'
                                                + '<a href="#" id="WIDGET_FULLSCREEN_HYSTORY">Toggle Fullscreen</a>'
                                            + '</li>'
                                            + '<li>'
                                                + '<a href="#" id="EXPORT_HYSTORY_TO_IMAGE">Export to image</a>'
                                            + '</li>'
                                            + '<li>'
                                                + '<a href="#" id="REMOVE_HISTORY">Remove</a>'
                                            + '</li>'
                                        + '</ul>'
                                    + '</li>'
                                + '</ul>'
                            + '</div>'
                            + '<div class="card-body m-t-0">'
                                + '<table class="table table-inner table-vmiddle">'
                                    + '<thead>'
                                        + '<tr>'
                                            + '<th>Epic</th>'
                                            + '<th>Time</th>'
                                            + '<th>Size</th>'
                                            + '<th>Levels</th>'
                                            + '<th>Pts</th>'
                                            + '<th>PNL</th>'
                                        + '</tr>'
                                    + '</thead>'
                                    + '<tbody id="TRADES_LIST">'
                                        + '<tr>'
                                            + '<td colspan="6" class="text-center">No trade today!</td>'
                                        + '</tr>'
                                    + '</tbody>'
                                + '</table>'
                            + '</div>'
                        + '</div>'
                + '</div>';
    $('#epicTradingCard').append(history); 
    populateHistoryTable();
    $('#REMOVE_HISTORY').on(clickEventTrigger, function(e){
        e.preventDefault();
        setTimeout(function(){
            $('#HYSTORY').remove();
        }, 500);
    });
    $('#WIDGET_FULLSCREEN_HYSTORY').on(clickEventTrigger, function(e){
        e.preventDefault();
        $('#HYSTORY').toggleClass('fullscreen-widget');
    });
    $('#EXPORT_HYSTORY_TO_IMAGE').on(clickEventTrigger, function(e){
        e.preventDefault();
        $(this).parents('.dropdown').removeClass('open');
        setTimeout(function(){
            html2canvas(document.getElementById('HYSTORY'), {
                  onrendered: function(canvas) {
                      var dt = canvas.toDataURL('image/jpeg');
                      downloadDataURI({
                            filename: 'History - ' + accountId + ' [' + $('#FROM_STATS').html() + ' | ' + $('#TO_STATS').html() + '].jpeg', 
                            data: dt
                    });
                  },
                  background: 'fff'
            });
        }, 700);
        /*html2canvas(document.getElementsByTagName('canvas'), {
            onrendered: function(canvas){
                $('body').append(canvas);
            }
        });*/
    });
});
    
    
    
$('#EXPORT_STATS_TO_PNG').on(clickEventTrigger, function(e){
    e.preventDefault();
    $('#GROWTH, #MONEY_TOTAL').toggleClass('blurry-text');
    html2canvas(document.getElementById('REPORT'), {
          onrendered: function(canvas) {
              var dt = canvas.toDataURL('image/jpeg');
              downloadDataURI({
                    filename: 'Report - ' + (isReal ? 'Live' : 'Demo') + ' [' + $('#FROM_STATS').html() + ' | ' + $('#TO_STATS').html() + '].jpeg',
                    data: dt
            });
              $('#GROWTH, #MONEY_TOTAL').toggleClass('blurry-text');
        },
        background: 'fff'
    });
});

/* dependency: jquery */
function downloadDataURI(options) {
      if(!options) {
        return;
      }
      $.isPlainObject(options) || (options = {data: options});
      /*if(!$.browser.webkit) {
        location.href = options.data;
      }*/
      options.filename || (options.filename = "download." + options.data.split(",")[0].split(";")[0].substring(5).split("/")[1]);
      options.url || (options.url = "https://download-data-uri.appspot.com/");
      $('<form method="post" action="/image/uri" style="display:none"><input type="hidden" name="filename" value="'+options.filename+'"/><input type="hidden" name="data" value="'+options.data+'"/></form>').submit().remove();
    }
   

function showTradingPane() {
    $('#landing').addClass("container-hidden");
    $('#landing').removeClass("container");
    $('#container').removeClass("container-hidden");
    $('#container').addClass("container");
}
});
