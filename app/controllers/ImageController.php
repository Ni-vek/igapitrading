<?php

namespace IGTrading\Controllers;

class ImageController extends BaseController
{

    public function uriAction()
    {
        if($this->request->isPost()){
            $datas = $this->request->getPost();
            $fp = fopen('exports/new-' . $datas['filename'], 'w');
            fwrite($fp, file_get_contents($datas['data']));
            fclose($fp);
            
            imagejpeg(imagecreatefromjpeg('exports/new-' . $datas['filename']), 'exports/' . $datas['filename'], 100);
            
            $size = filesize('exports/' . $datas['filename']);
            $quality = 85;
            while(filesize('exports/' . $datas['filename']) > 256000){
                unlink('exports/' . $datas['filename']);
                imagejpeg(imagecreatefromjpeg('exports/new-' . $datas['filename']), 'exports/' . $datas['filename'], $quality);
                $quality = $quality - 5;
            }
            $size = filesize('exports/' . $datas['filename']);
            
            header('Content-Description: File Transfer');
            header('Content-Type: image/jpeg');
            header('Content-Disposition: attachment; filename="' . $datas['filename'] . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . $size);
            readfile('exports/' . $datas['filename']);
            unlink('exports/new-' . $datas['filename']);
            unlink('exports/' . $datas['filename']);
            exit();
        }
    }

}
