<?php

namespace IGTrading\Controllers;

class NullController extends BaseController
{

    public function indexAction()
    {
        $this->view->disable();
    }

}