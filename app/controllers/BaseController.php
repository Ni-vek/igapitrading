<?php

namespace IGTrading\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;
use IGTrading\Models\Accounts;

class BaseController extends Controller
{

//    public function beforeDispatch(Dispatcher $dispatcher)
//    {
//        $actionName = $dispatcher->getActionName();
//        //Check if user has remember me!
//        if (!$this->session->has('auth-identity')) {
//            if ($this->auth->hasRememberMe()) {
//                if ($actionName !== 'logout' && $actionName !== 'lock') {
//                    if ($this->auth->loginWithRememberMe()) {
//                        
//                        return $this->response->redirect($this->request->getHTTPReferer());
//                    }
//                }
//            }
//        }
//    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();
        $actionName = $dispatcher->getActionName();
        
        //Check if user has remember me!
        if (!$this->session->has('auth-identity')) {
            if ($this->auth->hasRememberMe()) {
                if ($actionName !== 'logout' && $actionName !== 'lock') {
                    if ($this->auth->loginWithRememberMe()) {
                        if ($this->acl->isAllowed($this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest', $controllerName, $actionName)) {
                            $dispatcher->forward(array('controller' => $controllerName, 'action' => $actionName));
//                            return true;
                        }
                        elseif ($this->acl->isAllowed($this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest', $controllerName, 'index')) {
                            $dispatcher->forward(array('controller' => $controllerName, 'action' => 'index'));
//                            return true;
                        }
                        else {
                            $dispatcher->forward(array('controller' => 'dashboard', 'action' => 'index'));
//                            return true;
                        }
                    }
                    else {
                        $dispatcher->forward(array('controller' => 'index', 'action' => 'index'));
//                        return false;
                    }
                }
            }
            elseif ($this->auth->isLocked()) {
                if ($controllerName !== 'session' && $actionName !== 'lock' && $actionName !== 'unlock') {
                    $this->response->redirect('session/lock');
                }
            }
            else {
                if (!$this->acl->isAllowed($this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest', $controllerName, $actionName)) {
                    $dispatcher->forward(array('controller' => 'index'));
                }
            }
        }
        else {
            if ($controllerName == 'index') {
                $this->response->redirect('dashboard');
            }
        }

        // Check if the user have permission to the current option
        if (!$this->acl->isAllowed($this->auth->hasIdentity() ? $this->auth->getProfile()->getName() : 'Guest', $controllerName, $actionName)) {
            if ($this->auth->isLocked()) {
                if ($controllerName !== 'session' && $actionName !== 'lock' && $actionName !== 'unlock') {
                    $this->view->disable();
                    $this->response->redirect('session/lock');
                }
            }
            if ($this->session->has('auth-identity')) {
                if ($controllerName !== 'index') {
                    $dispatcher->forward(array(
                        'controller' => 'errors',
                        'action' => 'notFound'
                    ));
                }
                else {
                    $this->response->redirect('dashboard');
                }
            }
            else {
                $this->flash->error('You must be connected');
                $this->view->disable();
                $dispatcher->forward(array(
                    'controller' => 'index'
                ));
            }
        }
        else {
            if ($this->auth->isLocked()) {
                if ($controllerName !== 'session' && $actionName !== 'lock' && $actionName !== 'unlock') {
                    $this->view->disable();
                    $this->response->redirect('session/lock');
                }
            }
        }
    }

    public function initialize()
    {
        //Cheats to test if pjax is down or not
        /*if (APP_ENV !== 'prod') {
            sleep(1);
        }*/
        // Define template
        $this->view->setTemplateBefore('landing');
        
        //Set accounts
        if ($this->auth->hasIdentity()) {
            $this->view->setTemplateBefore('private');
            $this->view->setVar('accounts', Accounts::find(array(array(
                            'user' => $this->auth->getId(),
                            'deleted' => false),
                'sort' => array(
                    'name' => 1
                )
            )));
        }
        else {
            //$this->view->setTemplateBefore('landing');
        }

        //Retrieve each accounts
//        $this->view->setVar('accounts', Accounts::find());
        //Last part of the title
        $this->tag->setTitle($this->config->application->pageTitle/* . ' - ' . $this->config->application->siteSlogan*/);

        //App collection of CSS files
        $this->assets
                ->collection('appCss')
                ->addCss('css/app.min.1.css')
                ->addCss('css/app.min.2.css')
                ->addCss('css/jquery.contextMenu.css')
                ->addCss('css/font-awesome.min.css')
                ->addCss('https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic');
        
        //vendor collection of CSS files
        $this->assets
                ->collection('vendorCss')
                ->addCss('vendors/bower_components/animate.css/animate.min.css')
                ->addCss('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')
                ->addCss('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css');

        //Common scripts
        $this->assets
                ->collection('commonJs')
                ->addJs('vendors/bower_components/jquery/dist/jquery.min.js')
                ->addJs('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')
                ->addJs('vendors/bower_components/flot/jquery.flot.js')
                ->addJs('vendors/bower_components/flot/jquery.flot.resize.js')
                ->addJs('vendors/bower_components/flot.curvedlines/curvedLines.js')
                ->addJs('vendors/sparklines/jquery.sparkline.min.js')
                ->addJs('vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')
                ->addJs('vendors/bower_components/moment/min/moment.min.js')
                ->addJs('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')
                ->addJs('vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js')
                ->addJs('vendors/bower_components/Waves/dist/waves.min.js')
                ->addJs('vendors/bootstrap-growl/bootstrap-growl.min.js')
                ->addJs('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js')
                ->addJs('vendors/bower_components/typeahead.js/dist/typeahead.bundle.min.js')
                ->addJs('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')
                ->addJs('js/flot-charts/curved-line-chart.js')
                ->addJs('js/flot-charts/line-chart.js')
                ->addJs('js/html2canvas.js')
                ->addJs('js/bootstrap-editable.min.js')
                ->addJs('vendors/jquery-pjax/jquery.pjax.js');

        //App scripts
        $this->assets
                ->collection('appJs')
                ->addJs('js/buzz.min.js')
                ->addJs('js/aes.js')
                ->addJs('js/jquery.contextMenu.js')
                ->addJs('js/jquery.ui.position.js');
        
        //IG Scripts
        $this->assets
            ->collection('igJs')
            ->addJs('js/ig/pidder.js')
            ->addJs('js/ig/require.js')
            ->addJs('js/ig/beautifier.js')
            ->addJs('js/ig/lightstreamer.js');

	if (APP_ENV != 'prod') {
		$this->assets
		    ->collection('igJs')
		    ->addJs('js/ig/ig-public-api.js?v=' . $this->config->application->version);
        $this->assets
                ->collection('appJs')
                ->addJs('js/functions.js?v=' . $this->config->application->version);
        }
	else{

		$this->assets
		    ->collection('igJs')
		    ->addJs('js/ig/ig-public-api.min.js?v=' . $this->config->application->version);
        $this->assets
                ->collection('appJs')
                ->addJs('js/functions.min.js?v=' . $this->config->application->version);
	}

        // Pass the identity to each view called
        $this->view->setVar('identity', $this->auth->getIdentity());
    }

    public function validateMongoId($id)
    {
        if (preg_match('/^[0-9a-z]{24}$/', $id)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static function uuidV4()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}
