<?php

return new \Phalcon\Config(array(
    'application' => array(
        'controllersDir'            => APP_DIR . '/controllers/',
        'modelsDir'                 => APP_DIR . '/models/',
        'formsDir'                  => APP_DIR . '/forms/',
        'viewsDir'                  => APP_DIR . '/views/',
        'libraryDir'                => APP_DIR . '/library/',
        'pluginsDir'                => APP_DIR . '/plugins/',
        'cacheDir'                  => APP_DIR . '/cache/',
        'logDir'                    => BASE_DIR . '/var/log/',
        'baseUri'                   => '/',
        'siteTitle'                 => '<strong>{{ dev }}</strong> ',
        'pageTitle'                 => 'IG Trading{{ dev }}',
        'siteSlogan'                => '',
        'protocol'                  => 'http://',
        'publicUrl'                 => 'igtrading',
        'domain'                    => 'igtrading',
        'cryptSalt'                 => 'eEAdfgf:+.u>/6786M754@@s9~8_4L!<74@[NvU]:R|_&G&f41895681+--48841f156F5rdqd16@1ds7aIP_2My|jFr!!A&+71@Dy6m,$D',
        'isInMaintenance'           => false,
        'version'                   => '1.3.7'
    ),
    'cookies' => array(
        'expireTime'                => 86400 * 30
    ),
    'mongo' => array(
        'username'                  => null,
        'password'                  => null,
        'host'                      => null,
        'port'                      => 32172,
        'database'                  => 'igtrading'
    ),
    'mail' => array(
        'fromName'                  => 'IGTrading FX',
        'fromEmail'                 => 'noreply@anafx.com',
        'mandrillApiKey'            => 'HWk4p2D35PjYnpJrvxPFcg' //prod : HWk4p2D35PjYnpJrvxPFcg
    )
));
