<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces(
        array(
            'IGTrading'                     => $config->application->libraryDir,
            'IGTrading\Controllers'         => $config->application->controllersDir,
            'IGTrading\Models'              => $config->application->modelsDir,
            'IGTrading\Forms'               => $config->application->formsDir,
        )
)->register();
