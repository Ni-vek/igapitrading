<?php

namespace IGTrading\Acl;

use Phalcon\Mvc\User\Component;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Role as AclRole;
use Phalcon\Acl\Resource as AclResource;
use IGTrading\Models\Profiles;
use IGTrading\Models\Permissions;

class Acl extends Component
{

    /**
     * The ACL Object
     *
     * @var \Phalcon\Acl\Adapter\Memory
     */
    private $acl;

    /**
     * The filepath of the ACL cache file from APP_DIR
     *
     * @var string
     */
    private $filePath = '/cache/acl/data.txt';

    /**
     * Define the resources that are considered "private". These controller => actions require authentication.
     * The second parameter of each field defines if this action is free from subscription. true => free, false => not free 
     *
     * @var array
     */
    private $privateResources = array(
        'accounts' => array(
            'index',
            'add',
            'loadForm',
            'view',
            'delete',
            'update',
            '_is_updatable',
            '_retrieve_charts_datas'
        ),
        'administration' => array(
            'index'
        ),
//        'blog' => array(
//            'index'
//        ),
        'brokers' => array(
            'index',
            'add',
            'delete'
        ),
        'connectors' => array(
            'index',
            'add',
            'delete'
        ),
        'connectors_maps' => array(
            'index',
            'view',
            'add',
            'delete'
        ),
        'dashboard' => array(
            'index'
        ),
//        'database' => array(
//            'index'
//        ),
        'errors' => array(
            'notFound',
            'uncaughtException',
            'maintenance'
        ),
//        'faq' => array(
//            'index'
//        ),
        'index' => array(
            'index',
        ),
        'instruments' => array(
            'index',
            'add',
            'delete'
        ),
        'instruments_maps' => array(
            'index',
            'view',
            'add',
            'delete'
        ),
//        'mailbox' => array(
//            'index',
//            'ajaxSendNewMail',
//            'ajaxMessageView',
//            'ajaxMessageDeleter',
//            'ajaxMutipleRemover',
//            'ajaxMailboxResponseSender',
//            'quick'
//        ),
//        'notifications' => array(
//            'index'
//        ),
        'permissions' => array(
            'index'
        ),
//        'profiles' => array(
//            'index',
//            'edit',
//            'create',
//            'delete'
//        ),
        'session' => array(
            'signin',
            'register',
//            'password',
            'logout',
            'lock',
            'unlock',
//            'check'
        ),
//        'timeline' => array(
//            'index'
//        ),
        'user_control' => array(
            'confirmEmail',
            'resetPassword'
        ),
//        'users' => array(
//            'index'
//        )
    );

    /**
     * Human-readable descriptions of the actions used in {@see $privateResources}
     *
     * @var array
     */
    private $actionDescriptions = array(
    );

    /**
     * Checks if a controller is private or not
     *
     * @param string $controllerName
     * @return boolean
     */
    public function isPrivate($controllerName)
    {
        return isset($this->privateResources[$controllerName]);
    }

    /**
     * Checks if the current profile is allowed to access a resource
     *
     * @param string $profile
     * @param string $controller
     * @param string $action
     * @return boolean
     */
    public function isAllowed($profile, $controller, $action)
    {
         return true;
//        return $this->getAcl()->isAllowed($profile, $controller, $action);
    }

    /**
     * Returns the ACL list
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        // Check if the ACL is already created
        if (is_object($this->acl)) {
            return $this->acl;
        }

        // Check if the ACL is in APC
        if (function_exists('apc_fetch')) {
            $acl = apc_fetch('nannyster-acl');
            if (is_object($acl)) {
                $this->acl = $acl;
                return $acl;
            }
        }

        // Check if the ACL is already generated
        if (!file_exists(APP_DIR . $this->filePath)) {
            $this->acl = $this->rebuild();
            return $this->acl;
        }

        // Get the ACL from the data file
        $data = file_get_contents(APP_DIR . $this->filePath);
        $this->acl = unserialize($data);

        // Store the ACL in APC
        if (function_exists('apc_store')) {
            apc_store('nannyster-acl', $this->acl);
        }

        return $this->acl;
    }

    /**
     * Returns the permissions assigned to a profile
     *
     * @param Profiles $profile
     * @return array
     */
    public function getPermissions(Profiles $profile)
    {
        $profilePermissions = Permissions::find(array(array(
                        'profile_id' => $profile->getId())));
        $permissions = array();
        foreach ($profilePermissions as $permission) {
            $permissions[$permission->getResource() . '.' . $permission->getAction()] = true;
        }
        return $permissions;
    }

    /**
     * returns all permissions
     */
    public function getAllPermissions()
    {
        $permissions = Permissions::find();
        $array_permissions = array();
        foreach ($permissions as $permission) {
            $array_permissions[$permission->getResource() . '.' . $permission->getAction()][(string) $permission->getProfileId()] = true;
        }
        return $array_permissions;
    }

    /**
     * Returns all the resoruces and their actions available in the application
     *
     * @return array
     */
    public function getResources()
    {
        return $this->privateResources;
    }

    /**
     * Returns the action description according to its simplified name
     *
     * @param string $action
     * @return $action
     */
    public function getActionDescription($action)
    {
        if (isset($this->actionDescriptions[$action])) {
            return $this->actionDescriptions[$action];
        }
        else {
            return $action;
        }
    }

    /**
     * Rebuilds the access list into a file
     *
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(\Phalcon\Acl::DENY);

        // Register roles
        $profiles = Profiles::find(array(array(
                        'active' => true)));

        foreach ($profiles as $profile) {
            $acl->addRole(new AclRole($profile->getName()));
        }

        foreach ($this->privateResources as $resource => $actions) {
            $aclActions = array();
            foreach ($actions as $action) {
                $aclActions[] = $action;
            }
            $acl->addResource(new AclResource($resource), $aclActions);
        }
        
        // Grant acess to private area to role Users
        foreach ($profiles as $profile) {

            //Retrieve permissions for each profile
            $permissions = Permissions::find(array(array(
                            'profile_id' => $profile->getId())));

            // Grant permissions in "permissions" model
            foreach ($permissions as $permission) {
                $acl->allow($profile->getName(), $permission->getResource(), $permission->getAction());
            }

            // Always grant these permissions
//            $acl->allow($profile->getId(), 'users', 'changePassword');
        }

        if (touch(APP_DIR . $this->filePath) && is_writable(APP_DIR . $this->filePath)) {

            file_put_contents(APP_DIR . $this->filePath, serialize($acl));

            // Store the ACL in APC
            if (function_exists('apc_store')) {
                apc_store('nannyster-acl', $acl);
            }
        }
        else {
            $this->flash->error(
                    'The user does not have write permissions to create the ACL list at ' . APP_DIR . $this->filePath
            );
        }

        return $acl;
    }

}
