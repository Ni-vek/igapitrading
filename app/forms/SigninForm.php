<?php
namespace IGTrading\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;

class SigninForm extends Form
{

    public function initialize()
    {
        // Email
        $email = new Email('email', array(
            'placeholder' => 'Your Email',
            'class' => 'form-control input-lg'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your is required'
            )),
            new EmailValidator(array(
                'message' => 'Your email is not valid'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Your password',
            'class' => 'form-control input-lg'
        ));
        $password->addValidator(new PresenceOf(array(
            'message' => 'Your password is required'
        )));
        $this->add($password);
        
        // Remember
        $remember = new Check('remember', array(
            'value' => 'yes'
        ));
        $this->add($remember);
    }
}
