<?php
namespace IGTrading\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Check;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use IGTrading\Validator\UniqueValidator;

class RegisterForm extends Form
{

    public function initialize()
    {
        //Username
        $surname = new Text('username', array(
            'placeholder' => 'Your Username',
            'class' => 'form-control input-lg'
        ));
        $surname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your username is required'
            )),
            new UniqueValidator(array(
                'field' => 'username',
                'message' => 'This username is already used',
                'model' => 'Users'
            ))
        ));
        $this->add($surname);

        // Email
        $email = new Text('email', array(
            'placeholder' => 'Your Email',
            'class' => 'form-control input-lg'
        ));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your email is required'
            )),
            new Email(array(
                'message' => 'Your email is not valid'
            )),
            new UniqueValidator(array(
                'field' => 'email',
                'message' => 'This email is already used',
                'model' => 'Users'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password', array(
            'placeholder' => 'Your Password',
            'class' => 'form-control input-lg'
        ));
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Your password is required'
            )),
            new StringLength(array(
                'min' => 6,
                'messageMinimum' => 'Your password is too short. It should have 6 characters or more'
            )),
            new Confirmation(array(
                'message' => 'Passwords do not match',
                'with' => 'confirm_password'
            ))
        ));
        $this->add($password);

        // Confirm Password
        $confirm_password = new Password('confirm_password', array(
            'placeholder' => 'Confirm Your Password',
            'class' => 'form-control input-lg'
        ));
        $confirm_password->addValidators(array(
            new PresenceOf(array(
                'message' => 'The confirmation is required'
            ))
        ));
        $this->add($confirm_password);

        // Terms
        $terms = new Check('terms', array(
            'value' => 'yes'
        ));
        $terms->addValidator(new Identical(array(
            'value' => 'yes',
            'message' => 'Vous devez accepter les conditions d\'utilisations'
        )));
        $this->add($terms);

        // CSRF
//        $csrfSignUp = new Hidden('csrfRegister');
//        $csrfSignUp->addValidator(new Identical(array(
//            'value' => $this->security->getSessionToken(),
//            'message' => 'CSRF validation failed'
//        )));
//        $this->add($csrfSignUp);
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
